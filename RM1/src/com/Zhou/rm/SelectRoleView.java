package com.Zhou.rm;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import static com.Zhou.rm.Constant.*;

public class SelectRoleView extends GLSurfaceView{
	
	GL20Renderer mRenderer;
	MainActivity activity;
	Object3D currObj,huowuObj,GarenObj;
	List<Object3D> objs=new ArrayList<Object3D>();
	GL20Button back,start,sel_huowu,sel_Garen,background;
	List<GL20Button> buttons=new ArrayList<GL20Button>();
	public SelectRoleView(Context context) {
		super(context);
		activity=(MainActivity) context;
		LoadObject3Ds();
		LoadButtons();
		
		this.setEGLContextClientVersion(2);
		mRenderer=new GL20Renderer();
		this.setRenderer(mRenderer);
	}
	
	void LoadObject3Ds(){
		huowuObj=new Object3D(this,"huowu",6);
		objs.add(huowuObj);
		GarenObj=new Object3D(this,"Garen",1);
		objs.add(GarenObj);
		currObj=GarenObj;
	}
	void LoadButtons(){
		back=new GL20Button(0, 0, 200, 150, -1, R.drawable.ui_button_back, this);
		buttons.add(back);
		start=new GL20Button(0, 900, 200, 1080, -1, R.drawable.ui_button_ok, this);
		buttons.add(start);
		sel_huowu=new GL20Button(1680, 150, 1900, 400, -1, R.drawable.ui_select_huowu, this);
		buttons.add(sel_huowu);
		sel_Garen=new GL20Button(1680, 550, 1900, 800, -1, R.drawable.ui_select_garen, this);
		buttons.add(sel_Garen);
		background=new GL20Button(0, 0, 1920, 1080, -50, R.drawable.ui_background, this);
		buttons.add(background);
	}
	
	float prex,prey;
	final float rotate_speed=0.5f;
	@Override
	public boolean onTouchEvent(MotionEvent event){
		float mx=event.getX();
		float my=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
			//	activity.handler.sendEmptyMessage(ViewId.MenuView);
				for(GL20Button gb:buttons)
					gb.Pressed(mx, my);
				prex=mx;
				prey=my;
				break;
			case MotionEvent.ACTION_MOVE:
				currObj.angleX=currObj.pre_angleX+(my-prey)*rotate_speed;
				currObj.angleY=currObj.pre_angleY+(mx-prex)*rotate_speed;
				
				break;
			case MotionEvent.ACTION_UP:
				if(back.Release(mx, my))
					activity.handler.sendEmptyMessage(ViewId.MenuView);
				if(start.Release(mx, my))
					activity.handler.sendEmptyMessage(ViewId.GameView);
				if(sel_huowu.Release(mx, my))
					currObj=huowuObj;
				if(sel_Garen.Release(mx, my))
					currObj=GarenObj;
				currObj.pre_angleX=currObj.angleX;
				currObj.pre_angleY=currObj.angleY;
				
				break;
		} 
		return true;
	}
	
	class GL20Renderer implements Renderer{
		
		@Override
		public void onDrawFrame(GL10 gl) {
			GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT|GLES20.GL_DEPTH_BUFFER_BIT);
			
			currObj.DrawSelf();
			for(GL20Button gb:buttons)
				gb.DrawSelf();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {
			GLES20.glViewport(0, 0, width, height);
			float ratio=(float) width/height;
			MatrixState.setProjectFrustum(-ratio, ratio, -1, 1, 1, 100);
			MatrixState.setCamera(0, 0, 0, 0, 0, -1, 0, 1, 0);
		}
		
		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			GLES20.glClearColor(0, 0, 0, 1);
			GLES20.glEnable(GLES20.GL_DEPTH_TEST);
			GLES20.glClearDepthf(1.0f);
		//	GLES20.glEnable(GLES20.GL_CULL_FACE);
			
			for(Object3D obj:objs){
				obj.GenTextures();
				obj.InitShader();
			}
			
			for(GL20Button gb:buttons){
				gb.GenTexture();
				gb.InitShader();
			}
		}
		
		
	}
}
package com.Zhou.rm;

import java.io.IOException;
import java.util.HashMap;

import com.Zhou.rm.Constant.ViewId;
import com.Zhou.rm.novice.GameView2;
import com.Zhou.rm.novice.ScoreView2;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import static com.Zhou.rm.Constant.*;

public class MainActivity extends Activity {

	public Handler handler;
	
	WelcomeView welV;
	GameView gameV;
	MenuView menuV;
	SettingView setV;
	SelectRoleView selrV;
	ScoreView scoV;
	GameView2 novgmV;
	ScoreView2 novscoV;
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
    	if(keyCode==KeyEvent.KEYCODE_BACK){
    	//	System.exit(0);
    		new ExitDialog(this);
    	}
		return super.onKeyDown(keyCode, event);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        
        SW=getWindowManager().getDefaultDisplay().getWidth();
        SH=getWindowManager().getDefaultDisplay().getHeight();
        Log.v("SW SH",String.valueOf(SW)+" "+String.valueOf(SH));
        
        resources=this.getResources();
        
     //   float maxMemory = (float)Runtime.getRuntime().maxMemory() / 1024 /1024;  
     //   Log.e("maxMemory", "Max memory is "+maxMemory+" MB");
       
        new Thread(
        	new Runnable(){
	        	public void run(){
	        		Looper.prepare();//不加不行，，，
	        		if(selrV!=null) return ;
	        		selrV=new SelectRoleView(MainActivity.this);
	        		loadingFinish=true;
	        	}
        	}
        ).start();
       
        
        //游戏过程中只允许调整多媒体音量，而不允许调整通话音量。
      	setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        try {
	        bgm=MediaPlayer.create(MainActivity.this, R.raw.ui_bgm);
			bgm.setLooping(true);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
      
        com.Zhou.rm.Constant.soundPool=new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
		com.Zhou.rm.Constant.mscIdMap.put(1, soundPool.load(this, R.raw.eatthings, 1));
		com.Zhou.rm.Constant.mscIdMap.put(2, soundPool.load(this, R.raw.click, 1));
		com.Zhou.rm.Constant.mscIdMap.put(3, soundPool.load(this, R.raw.boom, 1));
		com.Zhou.rm.Constant.mscIdMap.put(4, soundPool.load(this, R.raw.wind, 1));
		com.Zhou.rm.Constant.mscIdMap.put(5, soundPool.load(this, R.raw.spring_jump, 1));
	// 	AudioManager mgr=(AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
    //  float streamVolumeCurrent=mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
    // 	float streamVolumeMax=mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    //  Log.v("voice",String.valueOf(streamVolumeCurrent)+" "+String.valueOf(streamVolumeMax));
    //	mscVolume=streamVolumeCurrent/streamVolumeMax;
		//获取声音设置
    	SharedPreferences sp=this.getSharedPreferences("Sound", Context.MODE_PRIVATE);
    	bgmSound=sp.getBoolean("bgmSound", true);
    	buttonSound=sp.getBoolean("buttonSound", true);
    	gameSound=sp.getBoolean("gameSound", true);
    	if(bgmSound) bgmVolume=bgmVolumeMax;
    	else bgmVolume=0f;
    	if(buttonSound) buttonVolume=buttonVolumeMax;
    	else buttonVolume=0f;
    	if(gameSound) gameVolume=gameVolumeMax;
    	else gameVolume=0f;
		
    	diamondsCnt=GetDiamondsCnt(this, 0);
    	
    	com.Zhou.rm.Constant.ui_background2=LoadUtil.MyDecodeStream(R.drawable.ui_background2, SW, SH);
    	screenRect=new Rect(0,0,SW,SH);
    	
    	handler=new Handler(){
    		@Override
    		public void handleMessage(Message msg) {
    			super.handleMessage(msg);
    			switch(msg.what){
    				case ViewId.WelView:
    					GotoWelView();//欢迎界面
    					break;
    				case ViewId.GameView:
    					GotoGameView();
    					break;
    				case ViewId.MenuView:
    					GotoMenuView();
    					break;
    				case ViewId.SettingView:
    					GotoSettingView();
    					break;
    				case ViewId.SelectView:
    					GotoSelectRoleView();
    					break;
    				case ViewId.ScoreView:
    					GotoScoreView();
    					break;
    				case ViewId.NoviceGameView:
    					GotoNoviceGameView();
    					break;
    				case ViewId.NoviceScoreView:
    					GotoNoviceScoreView();
    					break;
    			}
    		}
    	};
        handler.sendEmptyMessage(ViewId.WelView);  
        
        
    }
    
    private void GotoWelView(){
    	if(welV==null){
    		welV=new WelcomeView(MainActivity.this);//?
    	}
    	setContentView(welV);
    }
    
    private void GotoGameView(){
    	if(gameV==null){
    		gameV=new GameView(this);
    	}
    	gameV.Reset();
    	setContentView(gameV);
    }
    
    private void GotoMenuView(){
    	if(menuV==null){
    		menuV=new MenuView(this);
    	}
    	menuV.Reset();
    	setContentView(menuV);
    }
    
    private void GotoSettingView(){
    	if(setV==null){
    		setV=new SettingView(this);
    	}
    	setV.Reset();
    	setContentView(setV);
    }
    private void GotoSelectRoleView(){
    	if(selrV==null){
    		Log.e("reload","reload");
    		selrV=new SelectRoleView(this);
    	}
    	setContentView(selrV);
    }
    private void GotoScoreView(){
    	if(scoV==null){
    		scoV=new ScoreView(this);
    	}
    	scoV.Reset();
    	setContentView(scoV);
    }
    public void GotoNoviceGameView(){
    	if(novgmV==null){
    		novgmV=new GameView2(this);
    	}
    	novgmV.Reset();
    	setContentView(novgmV);
    }
    public void GotoNoviceScoreView(){
    	if(novscoV==null){
    		novscoV=new ScoreView2(this);
    	}
    	novscoV.Reset();
    	setContentView(novscoV);
    }
    
    @Override
	protected void onDestroy() {
    	bgm.release();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		bgm.start();
		bgm.setVolume(bgmVolume, bgmVolume);//调整音量要在start后
		super.onResume();
	}

	@Override
	protected void onPause() {
		bgm.pause();
		super.onPause();
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}

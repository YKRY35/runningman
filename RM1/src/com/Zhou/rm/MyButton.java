package com.Zhou.rm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import static com.Zhou.rm.Constant.*;
import static com.Zhou.rm.LoadUtil.*;
public class MyButton{//android ����ϵ
	int _x,_y,_ax,_ay;//����x��y
	private final float scale=0.1f;
	private int state;//10 -> -10
	private final int state_max=10;
	private int resId;
	private Bitmap bitmap=null;
	public MyButton(int _x,int _y,int _ax,int _ay,int resId){
		this._x=_x*SW/1920;
		this._y=_y*SH/1080;
		this._ax=_ax*SW/1920;
		this._ay=_ay*SH/1080;
		this.resId=resId;
		state=-state_max;
	}
	public void Reset(){
		state=-state_max;
	}
	public boolean Pressed(float mx,float my){
		if(PInside(mx,my)){
			state=state_max;
			soundPool.play(mscIdMap.get(2), buttonVolume, buttonVolume, 1, 0, 1.0f);
			return true;
		}
		return false;
	}
	public boolean Release(float mx,float my){
		if(PInside(mx,my)){
			return true;
		}
		return false;
	}
	public boolean PInside(float mx,float my){
		return (mx>_x&&mx<_ax&&my>_y&&my<_ay);
	}
	public Rect GetRect(){
		int size=state_max-Math.abs(state);
		int dw=(int) ((_ax-_x)*scale/2/state_max*size);
		int dh=(int) ((_ay-_y)*scale/2/state_max*size);
		return new Rect(_x+dw, _y+dh, _ax-dw, _ay-dh);
	}
	public Bitmap GetBitmap(){
		if(bitmap==null){
		//	bitmap=MyDecodeResource(resId, _ax-_x, _ay-_y);
			bitmap=MyDecodeStream(resId, this._ax-this._x, this._ay-this._y);
			Log.e("bitmap size menu",String.valueOf((float)getBitmapSize(bitmap)/1024/1024+"MB"));
		}
		return bitmap;
	}
	public void DrawSelf(Canvas canvas){
		canvas.drawBitmap(GetBitmap(), null, GetRect(), null);
		if(state>-state_max) state--;
	}
}
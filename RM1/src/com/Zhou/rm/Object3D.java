package com.Zhou.rm;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;
import static com.Zhou.rm.Constant.*;

class Object3D{
	public FloatBuffer vertexBuffer;
	public FloatBuffer texCoorBuffer;
	public int vertexCount;
	
	private SelectRoleView view;
	
	public String dir;
	
	private int imgCnt;
	private Bitmap[] bitmaps;
	public HashMap<String,Integer> FNToTId=new HashMap<String,Integer>();
	private int[] textures;
	
	public float maxX,minX,midX;
	public float maxY,minY,midY;
	public float maxZ,minZ,midZ;
	
	public float angleX=0,angleY=0;
	public float pre_angleX=0,pre_angleY=0;
	
	private String mVertexShader;
	private String mFragmentShader;
	private int mProgram;
	private int maPositionHandle;
	private int maTextureHandle;
//	private int maColorHandle;
	private int muMVPMatrixHandle;
	
	public List<Block> blocks=new ArrayList<Block>();
	class Block{
		int l,r,texture;
		public Block(int l,int r,int texture){
			this.l=l;
			this.r=r;
			this.texture=texture;
		}
	}
	public void AddBlock(int l,int r,int texture){
		blocks.add(new Block(l,r,texture));
	}
	
	public Object3D(SelectRoleView v,String dir,int imgCnt){
		this.view=v;
		this.dir=dir;
		this.imgCnt=imgCnt;
		InitArray();
		LoadBitmaps();
		LoadUtil.Load3DModelFromFile(this, v);
	}
	private void InitArray(){
		bitmaps=new Bitmap[imgCnt];
		textures=new int[imgCnt];
	}
	private void LoadBitmaps(){//dir="huowu"
		Log.v("start load bitmaps","start");
		int cnt=0;
		final String resDir="model3D/"+dir;
		String[] files=LoadUtil.GetAllFilesFromAsset(resDir, view);
		for(int i=0;i<files.length;i++){//files[i]文件名，不含路径  xx.png
			String[] filesa=files[i].split("\\.");
			if(filesa[filesa.length-1].trim().equals("png")){
				bitmaps[cnt]=LoadUtil.LoadBitmapFromAsset(resDir+"/"+files[i], view);
				Log.e("object3D bitmap size",String.valueOf(files[i]+" "+(float)LoadUtil.getBitmapSize(bitmaps[cnt])/1024/1024+"MB"));
				FNToTId.put(files[i], cnt);
				cnt++;
			}
		}
	}
	public void GenTextures(){
		GLES20.glGenTextures(imgCnt, textures, 0);
		for(int i=0;i<imgCnt;i++){
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[i]);
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_NEAREST);
				//设置放大过滤为使用纹理中坐标最接近的若干个颜色，通过加权平均算法得到需要绘制的像素颜色
		    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
		        //设置环绕方向S，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
		    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
		        //设置环绕方向T，截取纹理坐标到[1/2n,1-1/2n]。将导致永远不会与border融合
		    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);
		    	//根据以上指定的参数，生成一个2D纹理
	
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmaps[i], 0);
		}
	}
	
	void InitShader(){
		mVertexShader=ShaderUtil.loadFromAssetsFile("sh/vertex.sh", view.getResources());
		mFragmentShader=ShaderUtil.loadFromAssetsFile("sh/frag.sh", view.getResources());
		mProgram=ShaderUtil.createProgram(mVertexShader, mFragmentShader);
		maPositionHandle=GLES20.glGetAttribLocation(mProgram, "aPosition");
	//	maColorHandle=GLES20.glGetAttribLocation(mProgram, "aColor");
		maTextureHandle=GLES20.glGetAttribLocation(mProgram, "aTexture");
		muMVPMatrixHandle=GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
	}
	public void DrawSelf(){//huowu 19 -29 82 0 17 -67 max min X Y Z
		GLES20.glUseProgram(mProgram);
		GLES20.glEnableVertexAttribArray(maPositionHandle);
		GLES20.glEnableVertexAttribArray(maTextureHandle);
		MatrixState.setInitStack();
		float scale=2.0f/(maxY-minY);
	/*	Log.v("maxX",String.valueOf(maxX));
		Log.v("minX",String.valueOf(minX));
		Log.v("maxY",String.valueOf(maxY));
		Log.v("minY",String.valueOf(minY));
		Log.v("maxZ",String.valueOf(maxZ));
		Log.v("minZ",String.valueOf(minZ));*/
	//	MatrixState.scale(scale, scale, scale);
	//	MatrixState.scale(scale, scale, scale);
	//	float disZ=(maxY-minY)/2*1;
	//	MatrixState.translate(-midX, -midY, -maxZ-disZ);
		
	//	MatrixState.translate(0, 0, -(maxZ-minZ)/2-1);
		MatrixState.translate(0, 0, scale*(-(maxZ-minZ)/2)-1.5f);
		MatrixState.scale(scale, scale, scale);
		MatrixState.rotate(angleX, 1, 0, 0);
		MatrixState.rotate(angleY, 0, 1, 0);
		MatrixState.translate(-midX, -midY, -midZ);
		
		GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, MatrixState.getFinalMatrix(), 0);
		GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
		GLES20.glVertexAttribPointer(maTextureHandle, 2, GLES20.GL_FLOAT, false, 0, texCoorBuffer);
		for(int i=0;i<blocks.size();i++){
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[blocks.get(i).texture]);//blocks.get(i).texture
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES, blocks.get(i).l, blocks.get(i).r-blocks.get(i).l);
		}
	}
}
package com.Zhou.rm;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;
import static com.Zhou.rm.Constant.*;
import static com.Zhou.rm.LoadUtil.*;

public class GL20Button{
	public float _x,_y,_ax,_ay;//android坐标系  转化后横*ratioX
	float _z;//深度
	Bitmap bitmap;
	private FloatBuffer vertexBuffer;
	boolean vertexBuffer_flag=false;
	SelectRoleView view;
	
	private int texture;
	private String mVertexShader;
	private String mFragmentShader;
	private int mProgram;
	private int maPositionHandle;
	private int maTextureHandle;
	private int muMVPMatrixHandle;
	private boolean pressed;
	
	public GL20Button(float _x,float _y,float _ax,float _ay,float _z,int bitmapId,SelectRoleView v){
		this._x=_x*SW/1920;
		this._y=_y*SH/1080;
		this._ax=_ax*SW/1920;
		this._ay=_ay*SH/1080;//适应手机
		this._z=_z;
		this.view=v;
		pressed=false;
		CalcVertexBuffer();
		bitmap=MyDecodeStream(bitmapId, this._ax-this._x, this._ay-this._y);
	//	Log.e("bitmap size GL20Button",String.valueOf((float)getBitmapSize(bitmap)/1024/1024+"MB"));
	//	bitmap=MyDecodeResource(bitmapId, _ax-_x, _ay-_y);
	//	bitmap=BitmapFactory.decodeResource(v.getResources(), bitmapId);
	//	Log.e("bitmap size",String.valueOf((float)getBitmapSize(bitmap)/1024/1024+"MB"));
	//	bitmap=ScalingBitmap(bitmap, _ax-_x, _ay-_y);
	//	Log.e("bitmap size",String.valueOf((float)getBitmapSize(bitmap)/1024/1024+"MB"));
	}
	
	public void Pressed(float mx,float my){
		if(mx>=_x&&mx<=_ax&&my>=_y&&my<=_ay){
			soundPool.play(mscIdMap.get(2), buttonVolume, buttonVolume, 1, 0, 1.0f);
			
			pressed=true;
		}
	}
	public boolean Release(float mx,float my){
		if(pressed&&mx>=_x&&mx<=_ax&&my>=_y&&my<=_ay){
			pressed=false;
			return true;
		}
		pressed=false;
		return false;
	}
	
	public void CalcVertexBuffer(){
		float tx=((2*_x/SW-1)*SW/SH)*-_z;
		float ty=(-2*_y/SH+1)*-_z;
		float tax=((2*_ax/SW-1)*SW/SH)*-_z;
		float tay=(-2*_ay/SH+1)*-_z;
		float[] vertex={
				tx,ty,_z,
				tax,ty,_z,
				tx,tay,_z,
				tax,tay,_z
		};
		if(!vertexBuffer_flag){
			ByteBuffer BB=ByteBuffer.allocateDirect(vertex.length*4);
			BB.order(ByteOrder.nativeOrder());
			vertexBuffer=BB.asFloatBuffer();
			
			vertexBuffer_flag=true;
		}
		vertexBuffer.put(vertex);
		vertexBuffer.position(0);
	}
	
	public void GenTexture(){
		int[] textures=new int[1];
		GLES20.glGenTextures(1, textures, 0);
		texture=textures[0];
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);

		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
	}
	
	public void InitShader(){
		mVertexShader=ShaderUtil.loadFromAssetsFile("sh/vertex.sh", view.getResources());
		mFragmentShader=ShaderUtil.loadFromAssetsFile("sh/frag.sh", view.getResources());
		mProgram=ShaderUtil.createProgram(mVertexShader, mFragmentShader);
		maPositionHandle=GLES20.glGetAttribLocation(mProgram, "aPosition");
		maTextureHandle=GLES20.glGetAttribLocation(mProgram, "aTexture");
		muMVPMatrixHandle=GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
	}
	
	public void DrawSelf(){
		GLES20.glUseProgram(mProgram);
		GLES20.glEnableVertexAttribArray(maPositionHandle);
		GLES20.glEnableVertexAttribArray(maTextureHandle);
		MatrixState.setInitStack();
		GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, MatrixState.getFinalMatrix(), 0);
		GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
		GLES20.glVertexAttribPointer(maTextureHandle, 2, GLES20.GL_FLOAT, false, 0, GetTexCoordBuffer());
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
	}
}
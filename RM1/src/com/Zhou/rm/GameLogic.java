package com.Zhou.rm;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.Zhou.rm.gmobj.Box;
import com.Zhou.rm.gmobj.GameObject;
import com.Zhou.rm.gmobj.Ghost;
import com.Zhou.rm.gmobj.Ground;
import com.Zhou.rm.gmobj.Man;
import com.Zhou.rm.gmobj.ManState;
import com.Zhou.rm.gmobj.Missile;
import com.Zhou.rm.gmobj.Money;
import com.Zhou.rm.gmobj.Prop;
import com.Zhou.rm.gmobj.Spring;
import com.Zhou.rm.gmobj.Tower;

import android.content.Context;


import android.util.Log;
import static com.Zhou.rm.Constant.*;

public class GameLogic{
	
	public static GameLogic instance;
	public static GameLogic GetInstance(){
		if(instance==null){
			instance=new GameLogic();
		}
		return instance;
	}
	
	Man man;
	
	List<Ground> grounds=new ArrayList<Ground>();
//	List<Obstacle> obstacles=new ArrayList<Obstacle>();//障碍物集合
	List<GameObject> objects=new ArrayList<GameObject>();
	List<GameObject> foods=new ArrayList<GameObject>();//存储money，prop
//	List<Food> foods=new ArrayList<Food>();
	
	int level;
	int levelMax=5;
	public float stageSpeed;
	public float stageSpeedInc;//舞台速度增量
	public int stageSpeedInc_time;//舞台速度增量持续时间
	public float meters;
	
	public boolean reset_flag=true;
	public void Reset(){
		level=0;
		stageSpeed=-17f;
		groundsPreCnt=0;
		LoadMap();
		reset_flag=false;
		stageSpeedInc=0;
		stageSpeedInc_time=0;
		meters=0;
	}
	public void Revive(){
		man.ChangePosition(mapL, mapT);
		man.state.Revive();
		man.state.Flying(3*60, this);
		man.Revive();
	}
	int groundsPreCnt;
	private void LoadMap(){
		
		if(level>0) groundsPreCnt=grounds.size();
		if(level==0){
			grounds.clear();
	//		obstacles.clear();
	//		foods.clear();
			objects.clear();
			foods.clear();
			Ground g=new Ground(mapL+mapW/4,10*mapB,10*mapW,10*mapH);
			g.TurnOffVisible();
			grounds.add(g);
			objects.add(g);
	//		obstacles.add(g);
			groundsPreCnt=1;
		}
		
		GetData("gnd");
		GetData("box");
		GetData("tower");
		GetData("spring");
		GetData("ghost");
		GetData("prop");
		GetData("missile");
		
		for(int i=0;i<150;i++){
			GameObject f=new Money(mapR+i*300,300);
			foods.add(f);
		}
		if(level==0){
			man=new Man(mapL,grounds.get(1)._ay-grounds.get(1)._cay-Man._cy);
		}
	}
	
	Random random=new Random();
	private void GetData(String fileName){
		final String path="map/mpl"+String.valueOf(level)+"/",suffix=".txt";
		
		try {
			InputStream in=resources.getAssets().open(path+fileName+suffix);//文件流下次是否会初始化
			Scanner sc=new Scanner(in,"UTF-8");
			float last=mapL;////
			if(level>0) last=grounds.get(grounds.size()-1)._ax;//以grounds为标准
			int dataCnt=0;//第几行数据
			while(sc.hasNextInt()){
				if(fileName.equals("gnd")){
					float x1=last+sc.nextInt();
					float y1=sc.nextInt();//左上角
					float w1=sc.nextInt();
					float h1=sc.nextInt();
					Ground g=new Ground(x1,y1-h1,w1,h1);
					grounds.add(g);
					objects.add(g);
					last=x1+w1;
				}else{
					int gId=sc.nextInt();
					float x1=grounds.get(groundsPreCnt+gId)._x+sc.nextInt();
					float y1=sc.nextInt();
					
					GameObject go=null;
					if(fileName.equals("box")){//左下角
						go=new Box(x1,y1);
					}
					if(fileName.equals("tower")){//左下角
						go=new Tower(x1,y1);
					}
					if(fileName.equals("spring")){
						go=new Spring(x1,y1,dataCnt%2==0?1:-1);
					}
					
					if(fileName.equals("ghost")){
						go=new Ghost(x1,y1);
					}
					if(fileName.equals("missile")){
						go=new Missile(x1,y1);
					}
					if(go!=null){
						objects.add(go);
						continue;
					}
					
					if(fileName.equals("prop")){
						go=new Prop(x1,y1,random.nextInt());
					}
					foods.add(go);
				}
				
				dataCnt++;
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void CalcFrame(GameView v){
		if(!man.state.show) return ;
		if(man.state.dead){
			if(man.state.deadShow){
				v.gamePause=true;
				v.gameOver=true;
			}
			return ;
		}
		float _stageSpeed=stageSpeed;//总速度
		if(stageSpeedInc_time>0){
			_stageSpeed+=stageSpeedInc;
			stageSpeedInc_time--;
		}
		meters+=-_stageSpeed;
		man.state.UpdateState(man);
		
		man.GoAhead();
		for(GameObject o:objects){
			o.GoAhead(_stageSpeed);//物体左移
			o.CollideL(man);//障碍物左推man
		}
		man.GBArr();//调整移动距离
		for(GameObject o:foods){
			o.GoAhead(_stageSpeed,man);
			o.Eated(man,this);//吃食物
		}
		
		man.state.AdjV(man);
		if(man.state._v<0){//man向下
			man.GoDown();
			for(GameObject o:objects){
				o.CollideT(man);
			}
			man.FUArr(this);
			for(GameObject o:foods){
				o.VerticleMoving(man);
				o.Eated(man,this);//吃食物
			}
		}else{
			man.GoUp();
			for(GameObject o:objects){
				o.CollideB(man);
			}
			man.FDArr();
			for(GameObject o:foods){
				o.VerticleMoving(man);
				o.Eated(man,this);//吃食物
			}
		}
		
		man.GetSqtState(objects);//结束时产生了collider，无影子
									//东西可能穿过而未被检测   ---NO
		
	/*	for(GameObject go:objects){
			go.Eated(man);
		}*/
		
		UpdateList();
		ChangeLevel();
		
	}
	
	private void UpdateList(){//obstacles.size()=600会卡，图像抖动
		for(int i=1;i<objects.size();i++){
			if(objects.get(i)._ax<mapL){
				objects.remove(i);
				i--;
			}
		}
		
		for(int i=0;i<foods.size();i++){
			if(foods.get(i)._ax<mapL){
				foods.remove(i);
				i--;
			}
		}
		
		while(grounds.size()>1&&grounds.get(1)._ax<mapL){
			grounds.remove(1);
		}
		
	/*	Log.v("objects",String.valueOf(objects.size()));
		Log.v("grounds",String.valueOf(grounds.size()));
		Log.v("foods",String.valueOf(foods.size()));*/
	}
	
	private void ChangeLevel(){
		int i=grounds.size()-1,cnt=0;
		while(i>=1&&grounds.get(i)._ax>=mapR){
			cnt++;
			i--;
		}
		if(cnt==1){//加载地图 
			if(level+1<levelMax){
				level++;
				stageSpeed+=-1.5f;
				man.state._r=-stageSpeed/4;
				
			//	incStageSpeed=stageSpeed;
		//		tmpSpeedChange=0;
				
		//		backgndSpeed=-manSpeedR;
		//		missileSpeed=stageSpeed/2;
			}
			LoadMap();
			
		}
	}
}
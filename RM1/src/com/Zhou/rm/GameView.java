package com.Zhou.rm;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.MotionEvent;
import static com.Zhou.rm.Constant.*;

import com.Zhou.rm.BtnType;
import com.Zhou.rm.gmobj.GameObject;
import com.Zhou.rm.gmobj.ManState;

public class GameView extends GLSurfaceView{

	GameLogic GmL;
	private GL10Button jmpBtn,sqtBtn,restartBtn,fasterBtn,slowerBtn,resetManBtn,pauseBtn;
	private GL10Button reviveBtn,exitBtn,backBtn,againBtn;
	private GL10Button skill1Btn;
	private List<GL10Button> buttons=new ArrayList<GL10Button>();
	private GameBorder border;
	private boolean isInit=false;
	public boolean gamePause=false,gameOver=false;
	DrawPerformance drawPerf;
	private int reviveTimes;
	private int reviveTimesMax=4;
	private final int[] reviveFee={
			5,10,50,999
	};
	private MainActivity activity;
	public GameView(Context context) {
		super(context);
		this.activity=(MainActivity) context;
		drawPerf=new DrawPerformance();
		InitButtons();
		Reset();//不能确定：在draw前实例化gameLogic.man
		border=new GameBorder();
		setRenderer(new MRenderer());
	}
	public void Reset(){
		isInit=false;//锁住屏幕，不能点击
		GmL=GameLogic.GetInstance();
		GmL.Reset();//逻辑初始化
		gamePause=false;
		gameOver=false;
		for(GL10Button gb:buttons){
			gb.Reset();
		}
		reviveTimes=0;
	}
	private void InitButtons(){
		jmpBtn=new GL10Button(1600, 800, 1910, 1080, -1, R.drawable.game_jmp_btn85, R.drawable.game_jmp_btn55, ShowType.always);
		buttons.add(jmpBtn);
		sqtBtn=new GL10Button(10, 800, 320, 1080, -1, R.drawable.game_sqt_btn85, R.drawable.game_sqt_btn55, ShowType.always);
		buttons.add(sqtBtn);
		restartBtn=new GL10Button(0, 0, 200, 100, -1, R.drawable.game_box, ShowType.developer);
		buttons.add(restartBtn);
		fasterBtn=new GL10Button(400, 0, 600, 100, -1, R.drawable.game_box, ShowType.developer);
		buttons.add(fasterBtn);
		pauseBtn=new GL10Button(1720, 0, 1920, 100, -1, R.drawable.game_pause, ShowType.always);
		buttons.add(pauseBtn);
		slowerBtn=new GL10Button(800, 0, 1000, 100, -1, R.drawable.game_box, ShowType.developer);
		buttons.add(slowerBtn);
		resetManBtn=new GL10Button(1200,0,1400,100,-1,R.drawable.game_box, ShowType.developer);
		buttons.add(resetManBtn);
		
		reviveBtn=new GL10Button(1000,500,1300,800,-1,R.drawable.game_button_revive, ShowType.over);
		buttons.add(reviveBtn);
		againBtn=new GL10Button(600,500,900,800,-1,R.drawable.game_button_gotomainview, ShowType.over);//再来一局
		buttons.add(againBtn);
		exitBtn=new GL10Button(600,500,900,800,-1,R.drawable.game_button_gotomainview, ShowType.not_over__and_pause);
		buttons.add(exitBtn);
		backBtn=new GL10Button(1000,500,1300,800,-1,R.drawable.game_button_backtogame, ShowType.not_over__and_pause);
		buttons.add(backBtn);
		skill1Btn=new GL10Button(1400,770,1550,920,-1,R.drawable.game_skill1_85, R.drawable.game_skill1_55, ShowType.always);
		buttons.add(skill1Btn);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		if(!isInit) return false;
		int pointerId=GetPointerId(event.getAction());//第几根手指 会变
		int fingerId=event.getPointerId(pointerId);//手指索引 跟随手指不变
		float mx=event.getX(pointerId);
		float my=event.getY(pointerId);
		switch(event.getActionMasked()){
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				if(!gamePause&&GmL.man.state.show&&!GmL.man.state.dead){
					if(sqtBtn.Pressed(mx, my, fingerId, BtnType.sqt)){//蹲
						GmL.man.state.sqt_pressed=true;
					}else if(jmpBtn.Pressed(mx, my, fingerId, BtnType.jmp)){
						GmL.man.state.jmp_pressed=true;
						GmL.man.state.UserJmp();
						if(GmL.man.state.preOTG){
							GmL.man.state.sqt_pressed=false;
							sqtBtn.fingerId=-1;
						}
					}
					skill1Btn.Pressed(mx, my, fingerId, BtnType.skill1);
				}
				if(developerMode){
					if(restartBtn.Pressed(mx, my, fingerId, BtnType.restart)){
						GmL.reset_flag=true;
					}else if(fasterBtn.Pressed(mx, my, fingerId, BtnType.faster)){
						GmL.stageSpeed-=3;
					}else if(slowerBtn.Pressed(mx, my, fingerId, BtnType.slower)){
						if(GmL.stageSpeed+3<0)
							GmL.stageSpeed+=3;
					}else if(resetManBtn.Pressed(mx, my, fingerId, BtnType.reset_man)){
						GmL.man.ChangePosition(mapL,mapT);
					}
				}
				if(gameOver){
					reviveBtn.Pressed(mx, my, fingerId, BtnType.revive);
					againBtn.Pressed(mx, my, fingerId, BtnType.again);
				}
				if(gamePause&&!gameOver){
					exitBtn.Pressed(mx, my, fingerId, BtnType.exit);
					backBtn.Pressed(mx, my, fingerId, BtnType.back);
				}
				if(!gamePause&&pauseBtn.Pressed(mx, my, fingerId, BtnType.pause)){
					gamePause=true;
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				BtnType btnType=BtnType.nothing;
				for(GL10Button gb:buttons){
					btnType=gb.Release(fingerId,mx,my);
					if(btnType!=BtnType.nothing) break;
				}
				switch(btnType){
					case sqt:
						GmL.man.state.sqt_pressed=false;
						break;
					case jmp:
						GmL.man.state.jmp_pressed=false;
						break;
					case skill1://一技能
						GmL.man.state.UseSkill1(GmL);
						break;
					case exit:
						activity.handler.sendEmptyMessage(ViewId.MenuView);
						break;
					case back:
						gamePause=false;
						break;
					case revive:
						if(reviveTimes>=reviveTimesMax){
							DisplayToast("复活达到上限",activity);
						}else{
							if(diamondsCnt<reviveFee[reviveTimes]){
								DisplayToast("您的钻石不够",activity);
							}else{
								GetDiamondsCnt(activity,-reviveFee[reviveTimes]);
								diamondsCnt-=reviveFee[reviveTimes];
								GmL.Revive();
								gameOver=false;
								gamePause=false;
								reviveTimes++;
							}
						}
						break;
					case again:
						activity.handler.sendEmptyMessage(ViewId.ScoreView);
						break;
					default:
						break;
				}
				break;
		}
		return true;
	}
	private int GetPointerId(int action){
    	return (action&0xff00)>>8;
    }

	class MRenderer implements Renderer{
		
		@Override
		public void onDrawFrame(GL10 gl) {
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT);
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
			
			if(GmL.reset_flag) GmL.Reset();
		//	long startTime=System.currentTimeMillis();
			if(!gamePause)
				GmL.CalcFrame(GameView.this);
			Draw(gl);
		//	Log.v("CalcTime",String.valueOf(System.currentTimeMillis()-startTime));
		}
		
		public void Draw(GL10 gl){//顺序影响blend混合结果，可能有bug
			//画背景
			isInit=true;

		//	long startTime=System.currentTimeMillis();
			
			Draw4(gl,mapL*2,mapB*2,mapR*2,mapT*2,-2,R.drawable.game_bg1);//暂定最大深度为2

			
			for(GameObject go:GmL.objects){
				go.DrawSelf(gl);
			}
			
		//	Log.v("CalcTime",String.valueOf(System.currentTimeMillis()-startTime));
			
			for(GameObject go:GmL.foods){
				go.DrawSelf(gl);
			}
			GmL.man.DrawSelf(gl, gamePause);
			
			border.DrawSelf(gl);
			if(GmL.man.state.skill1Cd>0){
				float cdPercent=1.0f-(float)GmL.man.state.skill1Cd/(float)GmL.man.state.skill1CdMax;
				Draw4(gl,440,-380+150*cdPercent,590,-230,-1,R.drawable.game_skill_shade0);
			//	Log.v("cdPercent",String.valueOf(cdPercent));
			}
			if(GmL.man.state.ghostKiller!=0){//盾
				Draw4(gl,640,-180,790,-30,-1,R.drawable.game_skill_shield);
			}
			for(GL10Button gb:buttons){
				gb.DrawSelf(gl,gamePause,gameOver);
			}
			
			if(gameOver){//支付
				int r=R.drawable.game_revive_fee0+reviveTimes;
				Draw4(gl,110,90,350,350,-1,r);
			}
			
			drawPerf.DrawSelf(gl);
			
			
		}
		
		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {
			gl.glViewport(0, 0, width, height);
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			float ratio=(float)width/height;
		//	gl.glFrustumf(-ratio, ratio, -1, 1, 1, 100);
			gl.glFrustumf(-1, 1, -1, 1, 1, 100);
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			gl.glEnable(GL10.GL_DEPTH_TEST);
			gl.glClearDepthf(1.0f);
			gl.glDepthFunc(GL10.GL_LESS);
			gl.glEnable(GL10.GL_BLEND);
			gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
			gl.glClearColor(1, 0, 0, 1);
			
			LoadTexture(gl);
			drawPerf.GenTexture(gl);
		}
		
		void LoadTexture(GL10 gl){
			gl.glEnable(GL10.GL_TEXTURE_2D);
			gl.glGenTextures(texSize, textures, 0);
			for(int i=0;i<texSize;i++){
				gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[i]);
				int rr=fistResId+i;
				addTex(i,rr);
				Bitmap bm=LoadUtil.MyDecodeStream(rr);
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bm, 0);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
				bm.recycle();
			}
		}
	}
}
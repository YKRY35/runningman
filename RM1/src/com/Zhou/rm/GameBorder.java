package com.Zhou.rm;

import static com.Zhou.rm.Constant.*;
import static com.Zhou.rm.LoadUtil.*;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.BitmapFactory;

public class GameBorder{//逻辑坐标
	float drawL;
	final float speed=-5f;
	final float horH=((float)mapH-(float)mapH/-GmObjZ)/2;//顶部，底部高度
	float horW;//底部成比例的宽度
	final float verW=((float)mapW-(float)mapW/-GmObjZ)/2;
	public GameBorder(){
		BitmapFactory.Options option=new BitmapFactory.Options();
		option.inJustDecodeBounds=true;
		BitmapFactory.decodeResource(resources, R.drawable.game_border_horizontal, option);
		int picW=option.outWidth;
		int picH=option.outHeight;
		horW=(float)picW/(float)picH*horH;
		
		drawL=mapL;
	}
	
	
	public void DrawSelf(GL10 gl){
		float l=drawL;
		while(l<mapR){
			Draw4(gl, l, mapB, l+horW, mapB+horH, -1.0001f, R.drawable.game_border_horizontal);
			Draw4(gl, l, mapT-horH, l+horW, mapT, -1.0001f, R.drawable.game_border_horizontal);
			l+=horW;
		}
		drawL+=speed;
		if(drawL+horW<=mapL) drawL+=horW;
		
		Draw4(gl, mapL, mapB+horH, mapL+verW, mapT-horH, -1.0001f, R.drawable.game_border_verticle);
		Draw4(gl, mapR-verW, mapB+horH, mapR, mapT-horH, -1.0001f, R.drawable.game_border_verticle);
	}
}

package com.Zhou.rm;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.widget.Toast;

public class Constant{
	
	public static final int fistResId=R.drawable.game___+1;
	public static final int texSize=R.drawable.gamezzz-1-fistResId+1;//游戏里的纹理数量
	public static int[] textures=new int[texSize];//游戏中的纹理
	
	public static int SW;
	public static int SH;
	
	public static boolean loadingFinish=false;
	
	public static boolean developerMode=false;
	
	public static boolean bgmSound,buttonSound,gameSound;
	
	public static float bgmVolume,buttonVolume,gameVolume;
	public static final float bgmVolumeMax=0.35f,buttonVolumeMax=0.6f,gameVolumeMax=0.6f;
	
	public static MediaPlayer bgm;
	public static SoundPool soundPool;//游戏音效
	public static HashMap<Integer,Integer> mscIdMap=new HashMap<Integer,Integer>();
	
	public static int diamondsCnt;
	
	public static final int mapR=960;
	public static final int mapL=-960;
	public static final int mapT=540;
	public static final int mapB=-540;
	public static final int mapH=1080;
	public static final int mapW=1920;
	
	public static final float GmObjZ=-1.12f;//游戏元件z轴坐标
	
	public static Resources resources;
	
	public static Bitmap ui_background2;
	public static Rect screenRect;
	
	public static boolean CheckCollision(RectF r1,RectF r2){
		if(r2.right<=r1.left) return false;
		if(r2.bottom>=r1.top) return false;
		if(r2.left>=r1.right) return false;
		if(r2.top<=r1.bottom) return false;
		return true;
	}
	
	
	public static float LogicXToGLX(float x){//-1 -> 1
		return x/mapR;
	}
	public static float LogicYToGLY(float y){//逻辑坐标系转opengl坐标系
		return y/mapT;
	}
	
	public static float AndXToLogicX(float x){//android坐标系转化为逻辑坐标系
		return (2*x/SW-1)*mapR;
	}
	public static float AndYToLogicY(float y){
		return (-2*y/SH+1)*mapT;
	}
	
	public static int FitScreenX(int x){//XIAOMI的x转化为当前设备的x
		return x*SW/1920;
	}
	public static int FitScreenY(int y){
		return y*SH/1080;
	}
	
	public static int GetDiamondsCnt(Context context,int inc){
		SharedPreferences sp=context.getSharedPreferences("diamonds", Context.MODE_PRIVATE);
		int preCnt=sp.getInt("diamonds", 0);
		int newCnt=preCnt+inc;//增加以后数量
		Editor editor=sp.edit();
		editor.putInt("diamonds", newCnt);
		editor.commit();
		return preCnt;
	}
	
	public static boolean toastLock=false;
	public static synchronized void DisplayToast(String s,Context context){
		if(toastLock) return;
		toastLock=true;
		Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
		new Thread(){
			public void run(){
				try {
					Thread.sleep(2000);//2s后解锁
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				toastLock=false;
			}
		}.start();
	}
	
	private static FloatBuffer texCoordBuffer;
	private static boolean texCoordBuffer_flag=false;
	public static FloatBuffer GetTexCoordBuffer(){
		if(!texCoordBuffer_flag){
			float[] texCoord={
					0,0, 1,0, 0,1, 1,1 //左下，右下，左上，右上
			};
			ByteBuffer BB=ByteBuffer.allocateDirect(texCoord.length*4);
			BB.order(ByteOrder.nativeOrder());
			texCoordBuffer=BB.asFloatBuffer();
			texCoordBuffer.put(texCoord);
			texCoordBuffer.position(0);
			
			texCoordBuffer_flag=true;
		}
		return texCoordBuffer;
	}
	
	private static FloatBuffer vertexBuffer4;//4个三维float点
	private static boolean vertexBuffer4_flag=false;
	public static FloatBuffer GetVertexBuffer4(float x,float y,float ax,float ay,float z){//左下，右上
		float tx=LogicXToGLX(x);
		float ty=LogicYToGLY(y);
		float tax=LogicXToGLX(ax);
		float tay=LogicYToGLY(ay);
		float[] vertex={
				tx,tay,z,
				tax,tay,z,
				tx,ty,z,
				tax,ty,z
		};
		if(!vertexBuffer4_flag){
			ByteBuffer BB=ByteBuffer.allocateDirect(vertex.length*4);
			BB.order(ByteOrder.nativeOrder());
			vertexBuffer4=BB.asFloatBuffer();
			
			vertexBuffer4_flag=true;
		}
		vertexBuffer4.put(vertex);
		vertexBuffer4.position(0);
		return vertexBuffer4;
	}
	public static FloatBuffer GetVertexBuffer42(float x,float z,float ax,float az,float y){//左下，右上
		float tx=LogicXToGLX(x);
		float ty=LogicYToGLY(y);
		float tax=LogicXToGLX(ax);
		float[] vertex={
				tx,ty,az,
				tax,ty,az,
				tx,ty,z,
				tax,ty,z
		};
		if(!vertexBuffer4_flag){
			ByteBuffer BB=ByteBuffer.allocateDirect(vertex.length*4);
			BB.order(ByteOrder.nativeOrder());
			vertexBuffer4=BB.asFloatBuffer();
			
			vertexBuffer4_flag=true;
		}
		vertexBuffer4.put(vertex);
		vertexBuffer4.position(0);
		return vertexBuffer4;
	}
	public static FloatBuffer GetVertexBuffer43(float y,float z,float ay,float az,float x){//左下，右上
		float tx=LogicXToGLX(x);
		float ty=LogicYToGLY(y);
		float tay=LogicYToGLY(ay);
		float[] vertex={
				tx,ty,z,
				tx,ty,az,
				tx,tay,z,
				tx,tay,az
		};
		if(!vertexBuffer4_flag){
			ByteBuffer BB=ByteBuffer.allocateDirect(vertex.length*4);
			BB.order(ByteOrder.nativeOrder());
			vertexBuffer4=BB.asFloatBuffer();
			
			vertexBuffer4_flag=true;
		}
		vertexBuffer4.put(vertex);
		vertexBuffer4.position(0);
		return vertexBuffer4;
	}
	public static HashMap<Integer,Integer> texMap=new HashMap<Integer,Integer>();
	public static void addTex(int texId,int resId){
		texMap.put(resId, texId);
	}
	public static int GetTexture(int resId){
		int texId=texMap.get(resId);
		return textures[texId];
	}

	public static void Draw4(GL10 gl,float x,float y,float ax,float ay,float z,int resId){//平行屏幕矩形  
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, GetVertexBuffer4(x,y,ax,ay,z));
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, GetTexCoordBuffer());
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, GetTexture(resId));
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);//矩形
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
	
	public static void Draw42(GL10 gl,float x,float z,float ax,float az,float y,int resId){//垂直屏幕
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);										//游戏逻辑的坐标
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, GetVertexBuffer42(x,z,ax,az,y));
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, GetTexCoordBuffer());
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, GetTexture(resId));
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);//矩形
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
	public static void Draw43(GL10 gl,float y,float z,float ay,float az,float x,int resId){//平行y，z轴面
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);										//游戏逻辑的坐标
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, GetVertexBuffer43(y,z,ay,az,x));
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, GetTexCoordBuffer());
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, GetTexture(resId));
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);//矩形
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
	public static class ViewId{
		public static final int WelView=0;
		public static final int MenuView=1;
		public static final int SelectView=2;
		public static final int GameView=3;
		public static final int SettingView=4;
		public static final int ScoreView=5;
		public static final int NoviceGameView=6;
		public static final int NoviceScoreView=7;
	}
	public static enum GameObj{
		ground,box,spring
	};
	public static enum Dir{
		left,up,right,down
	}
	
	
}
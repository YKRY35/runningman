package com.Zhou.rm;

import com.Zhou.rm.Constant.ViewId;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import static com.Zhou.rm.Constant.*;

public class WelcomeView extends View{

	public WelcomeView(Context context) {
		super(context);
		activity=(MainActivity) context;
	}
	
	MainActivity activity;
	Paint paint;
	boolean isInit=false;
	Bitmap title;
	Rect titleRect;
	int titleAlpha;
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(!isInit){
			paint=new Paint();
		//	title=BitmapFactory.decodeResource(getResources(), R.drawable.title);
			title=LoadUtil.MyDecodeStream(R.drawable.title, SW/2, SH/2);
			titleRect=new Rect(SW/4, SH/4, SW*3/4, SH*3/4);
			titleAlpha=255;
			
			isInit=true;
		}
		Draw(canvas);
//		Log.v("Wel Draw","Wel Draw");
		postInvalidate();
	}
	private void Draw(Canvas canvas){
		canvas.drawColor(Color.BLACK);
		paint.setAlpha(titleAlpha);
		if(title!=null)
			canvas.drawBitmap(title, null, titleRect, paint);
		if(titleAlpha>1) titleAlpha-=2;
		else titleAlpha=0;
		
		if(titleAlpha==0){//����
			activity.handler.sendEmptyMessage(ViewId.MenuView);
			if(title!=null&&!title.isRecycled()){
				title.recycle();
				title=null;
			}
		}
	}
	
}
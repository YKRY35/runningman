//2017/2/3 21:xx 启动
package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.novice.GameLogic2;

import android.graphics.RectF;
import android.util.Log;

//逻辑里，横屏中心为原点，右边x正，上边y正，符合opengl坐标系
public class Man{//左下，右上
	public float _x,_y,_ax,_ay;//整张图片坐标
	public final float sqt_w=200,sqt_h=71/53*sqt_w;//蹲下图片宽高
	public static final float _w=300,_h=260;
	public static final float _cx=_w/10,_cy=0,_cax=_w/10,_cay=_h/10;//碰撞检测坐标差值,向图片内收缩
//	public Speed speed;
	public RectF collider;
	private final float bigFloat=100000000;
	private final float smaFloat=-100000000;	
	private float min_o_l;
	private float max_o_t;
	private float min_o_b;
	private GameObject vType;
	public ManState state;
	
	public int moneyCnt,killGhostCnt,wreckBoxCnt;
	
	public Man(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		state=new ManState(3);
		moneyCnt=0;
		killGhostCnt=0;
		wreckBoxCnt=0;
	}
	public void ChangePosition(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
	}
	
	
	public void GoAhead(){
		GetCollider(_x+_cx, _ay-_cay, _ax-_cax+state._r, _y+_cy);//left top right bottom
		min_o_l=bigFloat;
	//	TranslateX(speed._r);
	}
	
	public void GoBackward(float o_l,GameObject type){//object left
		if(o_l<min_o_l){
			min_o_l=o_l;
			vType=type;
		}
	}
	public void GBArr(){
		if(min_o_l==bigFloat){//向右没碰到物体
			TranslateX(state._r);//collider正确
		}else{
			float dis=min_o_l-(_ax-_cax);
			TranslateX(dis);
			vType.ManGBArr(this);
			if(dis>0){//向右运动
				GetCollider(_x+_cx, _ay-_cay, min_o_l, _y+_cy);
			}else{//静止或向左
				GetCollider(_x+_cx+dis, _ay-_cay, _ax-_cax, _y+_cy);
			}
		}
	}
	
	
	
	public void GoDown(){//_v<0
		GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+state._v);
	//	TranslateY(speed._v);
		max_o_t=smaFloat;
	}
	public void GoUp(){
		GetCollider(_x+_cx, _ay-_cay+state._v, _ax-_cax, _y+_cy);
	//	TranslateY(speed._v);
		min_o_b=bigFloat;
	}
	
	public void ForceUp(float o_t,GameObject type){//object top
		if(o_t>max_o_t){
			vType=type;
			max_o_t=o_t;
		}
	}
	public void FUArr(GameLogic GmL){//force up 整理
		if(max_o_t!=smaFloat){//踩到
			float dis=max_o_t-(_y+_cy);
			TranslateY(dis);
			if(dis>0){//向上位移
				GetCollider(_x+_cx, _ay-_cay+dis, _ax-_cax, _y+_cy);
			}else{
				GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+dis);
			}
			state.ResetJmp();
			vType.ManFUArr(this, GmL);
		
		}else{
			TranslateY(state._v);
			state._v+=state._a;
		}
	}
	public void FUArr2(GameLogic2 GmL){//force up 整理
		if(max_o_t!=smaFloat){//踩到
			float dis=max_o_t-(_y+_cy);
			TranslateY(dis);
			if(dis>0){//向上位移
				GetCollider(_x+_cx, _ay-_cay+dis, _ax-_cax, _y+_cy);
			}else{
				GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+dis);
			}
			state.ResetJmp();
			vType.ManFUArr2(this, GmL);
		
		}else{
			TranslateY(state._v);
			state._v+=state._a;
		}
	}
	public void ForceDown(float o_b,GameObject type){
		if(o_b<min_o_b){
			vType=type;
			min_o_b=o_b;
		}
	}
	public void FDArr(){//force down 整理
		if(min_o_b!=bigFloat){//顶到
			float dis=min_o_b-(_ay-_cay);
			TranslateY(dis);
			if(dis>0){//向上位移
				GetCollider(_x+_cx, _ay-_cay+dis, _ax-_cax, _y+_cy);
			}else{
				GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+dis);
			}

			vType.ManFDArr(this);
		}else{
			TranslateY(state._v);
			state._v+=state._a;
		}
	}
	
/*	public void VerMoving(){//垂直移动
		if(speed._v<0){
			GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+speed._v);
		}else{
			GetCollider(_x+_cx, _ay-_cay+speed._v, _ax-_cax, _y+_cy);
		}
		TranslateY(speed._v);
		speed._v+=speed._a;
	}
	*/
	public void GetSqtState(List<GameObject> objects){
	//	speed.jmpLock=false;//在柱子下可以跳跃卡了这段代码的运行时间
		if(state.sqt_pressed){//用户欲蹲
			if(state.OTG){
				state.sqt=true;
			}else{
				state.sqt=false;
			}
		}
		state.preOTG=state.OTG;
		state.OTG=false;
		
		boolean jmpLock=false;
		if(state.sqt){//检查是否能站起
			boolean coll=false;
			collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
			for(GameObject go:objects){//障碍物
				if(go.CollideA(this)){
					coll=true;
					break;
				}
			}
			if(coll){
			//	speed.jmpLock=true;
				jmpLock=true;
			}else if(!state.sqt_pressed){
				state.sqt=false;
			}
		}
		state.jmpLock=jmpLock;
		GetCollider(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
	}
	
	private void GetCollider(float l,float t,float r,float b){
		collider=new RectF(l,t,r,b);
		if(state.sqt){
			collider.top-=_h-ManState._h;
		}
	}
	/*
	class PicId{
		public PicId(){
			run_reset();
			sqt_reset();
		}
		
		public static final int k=5;//k帧换一次图
		private int _runId,_sqtId;
		public int runId,sqtId;//for
		public static final int runSize=6,sqtSize=6;
		public void run_reset(){
			_runId=0;
			runId=0;
		}
		public void run_inc(){
			_runId++;
			if(_runId>=runSize*k) _runId=0;
			runId=_runId/k;
		}
		public void sqt_reset(){
			_sqtId=0;
			sqtId=0;
		}
		public void sqt_inc(){
			_sqtId++;
			if(_sqtId>=sqtSize*k) _sqtId=0;
			sqtId=_sqtId/k;
		}
	}*/
	//PicId picId;
	public void Revive(){
		p_run.Reset();
		p_sqt.Reset();
		p_fly.Reset();
		p_show.Reset();
		p_dead.Reset();;
	}
	class Scroll{
		public int num;//当前数字
		public int num_max;
		private int k;//num变换速度
		private int k_now;
		public Scroll(int num_max,int k){
			this.num_max=num_max;
			this.k=k;
			this.num=0;
			this.k_now=0;
		}
		private void run(){
			k_now++;
			if(k_now==k){
				k_now=0;
				num=(num+1)%num_max;
			}
		}
		public int GetNum(){
			run();
			return num;
		}
		public void Reset(){
			this.num=0;
			this.k_now=0;
		}
	}
	Scroll p_run,p_sqt,p_fly,p_show,p_dead;
	public void DrawSelf(GL10 gl, boolean gamePause){
		if(p_run==null) p_run=new Scroll(6,5);
		if(p_sqt==null) p_sqt=new Scroll(6,5);
		if(p_fly==null) p_fly=new Scroll(7,5);
		if(p_show==null) p_show=new Scroll(42,5);
		if(p_dead==null) p_dead=new Scroll(10,5);
		int r=R.drawable.game_man_run0;
	//	Draw4(gl,_x,_y,_ax,_ay,R.drawable.man);
		if(state.dead){
			r=R.drawable.game_man_dead09;
			if(!state.deadShow){
				int num=p_dead.GetNum();
				r=R.drawable.game_man_dead00+num;
				Draw4(gl, _x, _y, _x+127*3f, _y+108*3f, GmObjZ, r);
				if(num==9) state.deadShow=true;
				return ;
			}
		}else if(!state.show){
			int num=p_show.GetNum();
			r=R.drawable.game_man_show00+num;
			Draw4(gl, _x, _y, _x+141*3, _y+153*3, GmObjZ, r);
			if(num==41){
				state.show=true;
			}
			return ;
		}else if(state.flying!=0){//飞行
			r=R.drawable.game_man_flying0+p_fly.GetNum();
			Draw4(gl, _ax-1.3917f*_w, _y, _ax, _y+_h*1.1509f, GmObjZ, r);
			return ;
		}else if(state.jmpCnt==0){//地上
			if(state.sqt){
				r=R.drawable.game_man_roll0+p_sqt.GetNum();
				Draw4(gl, _ax-sqt_w, _y, _ax, _y+sqt_h, GmObjZ, r);
				return ;
			}
			else{
				if(state.jmpBuff>0){
					r=R.drawable.game_man_jmp0;
				}else{
					r=R.drawable.game_man_run0+p_run.GetNum();
				}
			}
		}else if(state.jmpCnt==1){
			int fra=(int) ((state._v-ManState.jmp1)/state._a)/8;//一段跳后第fra帧 20/0.5/7
		//	Log.v("fra",String.valueOf(fra));
			if(fra>4) fra=4;
			r=R.drawable.game_man_jmp2+fra;
		}else if(state.jmpCnt==2||(state.jmpCnt==3&&state.glided==0)){
			int fra=(int) ((state._v-ManState.jmp2)/state._a)/8;
			if(fra>4) fra=4;
			r=R.drawable.game_man_jmp2+fra;
		}else if(state.glided==1){//正在滑翔
			int offset=state.gliFraCnt/5;//5帧换图
			if(offset<3){
				r=R.drawable.game_man_gli_fire0+offset;
				if(offset==0){
					Draw4(gl, _ax-1.3917f*_w, _y, _ax, _y+_h*1.1509f, GmObjZ, r);
				}else if(offset==1){
					Draw4(gl, _ax-1.4647f*_w, _y, _ax, _y+_h*1.1730f, GmObjZ, r);
				}else{
					Draw4(gl, _ax-1.6645f*_w, _y, _ax, _y+_h*1.1636f, GmObjZ, r);
				}
				return ;
			}else{
				r=R.drawable.game_man_glide1;//1.1246
				Draw4(gl, _ax-1.1246f*_w, _y, _ax, _ay, GmObjZ, r);
				return ;
			}
		}else if(state.glided!=1){//下落
			r=R.drawable.game_man_jmp5;
		}
		Draw4(gl, _x, _y, _ax, _ay, GmObjZ, r);
	}
}
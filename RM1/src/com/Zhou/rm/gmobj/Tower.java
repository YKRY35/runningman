package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.CheckCollision;
import static com.Zhou.rm.Constant.Draw4;
import static com.Zhou.rm.Constant.GmObjZ;
import static com.Zhou.rm.Constant.mapR;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.R;

import android.graphics.RectF;
import static com.Zhou.rm.Constant.*;

public class Tower extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=350,_h=700;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//��ײ��������ֵ,��ͼƬ������
	private RectF collider;
	int state;
	
	public Tower(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		this.state=0;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed){//speed<0
		collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
		TranslateX(speed);
	}
	
	public boolean CollideA(Man man){
		return state==0&&CheckCollision(man.collider,collider);
	}
	
	public void CollideL(Man man){//��Ҫʱ����man������
		if(state==0&&CheckCollision(man.collider,this.collider)){//��ײ
			if(man.state.bldgKiller==0)
				man.GoBackward(_x+_cx,this);
			else{
				state=1;
				SoundPlay();
			}
		}
	}
	public void SoundPlay(){
		soundPool.play(mscIdMap.get(3), gameVolume, gameVolume, 1, 0, 1.0f);
	}
	
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;
		if(state==0)
			Draw4(gl,_x,_y,_ax,_ay, GmObjZ, R.drawable.game_tower_blue);
		else{
			Draw4(gl, _x, _y, _ax, _ay+_h*0.375f, GmObjZ, R.drawable.game_boom00+state-1);
			if(state+1<25) state++;
		}
	}
	
}
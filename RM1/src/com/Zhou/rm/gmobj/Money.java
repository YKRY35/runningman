package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.CheckCollision;
import static com.Zhou.rm.Constant.Draw4;
import static com.Zhou.rm.Constant.mapR;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.MainActivity;
import com.Zhou.rm.R;
import com.Zhou.rm.Constant.GameObj;
import com.Zhou.rm.R.drawable;
import com.Zhou.rm.novice.GameLogic2;

import android.content.Context;
import android.graphics.RectF;
import android.media.AudioManager;
import static com.Zhou.rm.Constant.*;

public class Money extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=50,_h=50;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//��ײ��������ֵ,��ͼƬ������
	private RectF collider;
	public boolean visible;
	float mag_x,mag_y;
	final float mag_v=50f;
	public Money(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		super.SendToFather(_x, _y, _ax, _ay);
		visible=true;
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed,Man man){//speed<0
		if(_x<mapR&&man.state.magnetic!=0){//����ʯ
			float dx=man._x-_x,dy=man._y-_y;
			float dz=(float) Math.sqrt(dx*dx+dy*dy);
			mag_x=0;
			mag_y=0;
			if(dz!=0){
				mag_x=mag_v/dz*dx;
				mag_y=mag_v/dz*dy;
			}
			if(mag_x>0){
				collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax+mag_x, _y+_cy);
			}else{
				collider=new RectF(_x+_cx+mag_x, _ay-_cay, _ax-_cax, _y+_cy);
			}
			TranslateX(mag_x);
		}else{
			collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
			TranslateX(speed);
		}
	}
	public void VerticleMoving(Man man){
		if(_x<mapR&&man.state.magnetic!=0){
			if(mag_y>0){
				collider=new RectF(_x+_cx, _ay-_cay+mag_y, _ax-_cax, _y+_cy);
			}else{
				collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+mag_y);
			}
			TranslateY(mag_y);
		}else{
			collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		}
	}
	public void Eated(Man man,GameLogic GmL){
		if(visible&&CheckCollision(man.collider,this.collider)){
			man.moneyCnt++;
			visible=false;
		
			soundPool.play(mscIdMap.get(1), gameVolume/2, gameVolume/2, 1, 0, 1.0f);
		}
	}
	public void Eated2(Man man,GameLogic2 GmL){
		if(visible&&CheckCollision(man.collider,this.collider)){
			man.moneyCnt++;
			visible=false;
		
			soundPool.play(mscIdMap.get(1), gameVolume/2, gameVolume/2, 1, 0, 1.0f);
		}
	}
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;
		if(!visible) return ;
		Draw4(gl,_x,_y,_ax,_ay, GmObjZ,R.drawable.game_money);
	}
	
}
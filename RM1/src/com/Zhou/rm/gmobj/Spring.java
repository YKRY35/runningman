package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.Constant.GameObj;
import com.Zhou.rm.R.drawable;
import com.Zhou.rm.novice.GameLogic2;

import android.graphics.RectF;

public class Spring extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=170,_h=150;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//��ײ��������ֵ,��ͼƬ������
	private RectF collider;
	
	public Spring(float _x,float _y,int shake_state_inc){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		this.shake_state_inc=shake_state_inc;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed){//speed<0
		collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
		TranslateX(speed);
	}
	
	public void CollideL(Man man){//��Ҫʱ����man������
		if(CheckCollision(man.collider,this.collider)){//��ײ
			man.GoBackward(_x+_cx,this);
		}
	}
	
	public void CollideT(Man man){//�ϱ�����ײ���
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(CheckCollision(man.collider,this.collider)){
			man.ForceUp(_ay-_cay, this);
		}
	}
	
/*	public void CollideB(Man man){//�±�����ײ���
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(CheckCollision(man.collider,this.collider)){
			man.ForceDown(_y+_cy, GameObj.spring);
		}
	}*/
	public void ManFUArr(Man man, GameLogic GmL){
		man.state._v=ManState.springU;
		man.state.JmpOnce();
		GmL.stageSpeedInc=GmL.stageSpeed*3/2;
		GmL.stageSpeedInc_time=(int) (2*-ManState.springU/ManState.gra_a);
		
		soundPool.play(mscIdMap.get(5), gameVolume, gameVolume, 1, 0, 1.0f);
	}
	public void ManFUArr2(Man man, GameLogic2 GmL){
		man.state._v=ManState.springU;
		man.state.JmpOnce();
		GmL.stageSpeedInc=GmL.stageSpeed*3/2;
		GmL.stageSpeedInc_time=(int) (2*-ManState.springU/ManState.gra_a);
		
		soundPool.play(mscIdMap.get(5), gameVolume, gameVolume, 1, 0, 1.0f);
	}
	private int shake_state=0;
	int shake_state_inc,shake_state_max=10;
	private final float shake_h=1;
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;
		Draw4(gl,_x,_y,_ax,_ay+shake_state*shake_h, GmObjZ,R.drawable.game_spring);
		shake_state+=shake_state_inc;
		if(shake_state==shake_state_max) shake_state_inc=-1;
		if(shake_state==-shake_state_max) shake_state_inc=1;
	}
	
}
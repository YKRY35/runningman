package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.Constant.GameObj;
import com.Zhou.rm.novice.GameLogic2;

import android.graphics.BitmapFactory;
import android.graphics.RectF;

public class Ground extends GameObject{
	public float _x,_y,_w,_h;
	public float _ax,_ay;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//碰撞检测坐标差值,向图片内收缩
	private RectF collider;
	private boolean visible;
	
	public Ground(float _x,float _y,float _w,float _h){
		this._x=_x;
		this._y=_y;
		this._w=_w;
		this._h=_h;
		this._ax=_x+_w;
		this._ay=_y+_h;
		super.SendToFather(_x, _y, _ax, _ay);
		this.visible=true;
	}
	public void TurnOffVisible(){
		visible=false;
	}
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed){//speed<0
		collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
		if(visible) TranslateX(speed);
		else collider.left-=speed;
	}
	
/*	public boolean CollideA(Man man){
		return CheckCollision(man.collider,collider);
	}*/
	
	public void CollideL(Man man){//必要时，把man往左推
		if(CheckCollision(man.collider,this.collider)){//碰撞
			man.GoBackward(_x+_cx,this);
		}
	}
	
	public void CollideT(Man man){//上表面碰撞检测
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(CheckCollision(man.collider,this.collider)){
			man.ForceUp(_ay-_cay, this);
		}
	}
	public void ManFUArr(Man man, GameLogic GmL){
		man.state._v=ManState._vbeg;
		man.state.OTG=true;
	}
	public void ManFUArr2(Man man, GameLogic2 GmL){
		man.state._v=ManState._vbeg;
		man.state.OTG=true;
	}
	private final float zNear=-1.01f;//靠外的面深度
	private final float zFar=2*GmObjZ-zNear;
	private static float picW=-1,picH=-1;
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;
		if(!visible) return ;
		if(picW<=0||picH<=0){
			BitmapFactory.Options option=new BitmapFactory.Options();
			option.inJustDecodeBounds=true;
			BitmapFactory.decodeResource(resources, R.drawable.game_ground, option);
			picW=option.outWidth;
			picH=option.outHeight;
		}
		if(picW/picH<_w/_h){//水平画图
			float oneW=_h/picH*picW;
			int picCnt=Math.round(_w/oneW);
			oneW=_w/(float)picCnt;
			for(int i=0;i<picCnt;i++){
				Draw4(gl, _x+i*oneW, _y, _x+(i+1)*oneW, _ay, zNear, R.drawable.game_ground);
			}
		}else{//垂直画图
			float oneH=_w/picW*picH;
			int picCnt=Math.round(_h/oneH);
			oneH=_h/(float)picCnt;
			for(int i=0;i<picCnt;i++){
				Draw4(gl, _x, _y+i*oneH, _ax, _y+(i+1)*oneH, zNear, R.drawable.game_ground);
			}
		}
		
		float myW=251;
		int picCnt=Math.round(_w/myW);
		if(picCnt==0) picCnt=1;
		myW=_w/picCnt;
		for(int i=0;i<picCnt;i++){
			Draw42(gl,_x+i*myW, zNear ,_x+(i+1)*myW, zFar, _ay, R.drawable.game_ground2);
		}
		
	//	Draw4(gl, _x, _y, _ax, _ay, zNear, R.drawable.game_ground);
	//	Draw42(gl,_x, zNear ,_ax, zFar, _ay, R.drawable.game_ground);
		Draw43(gl, _y, zNear, _ay, zFar, _ax, R.drawable.game_ground);
		Draw43(gl, _y, zNear, _ay, zFar, _x, R.drawable.game_ground);
	}
	
}
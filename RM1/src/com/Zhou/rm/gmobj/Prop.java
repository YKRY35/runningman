package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.RectF;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.novice.GameLogic2;

public class Prop extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=150,_h=150;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//��ײ��������ֵ,��ͼƬ������
	private RectF collider;
	private int state;
	float mag_x,mag_y;
	final float mag_v=50f;
	int type;
	static final int type_amount=3;
	public Prop(float _x,float _y,int type){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		this.state=0;
		this.type=( (type%type_amount)+type_amount )%type_amount;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed,Man man){//speed<0
		if(_x<mapR&&man.state.magnetic!=0){//����ʯ,����Ļ��
			float dx=man._x-_x,dy=man._y-_y;
			float dz=(float) Math.sqrt(dx*dx+dy*dy);
			mag_x=0;
			mag_y=0;
			if(dz!=0){
				mag_x=mag_v/dz*dx;
				mag_y=mag_v/dz*dy;
			}
			if(mag_x>0){
				collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax+mag_x, _y+_cy);
			}else{
				collider=new RectF(_x+_cx+mag_x, _ay-_cay, _ax-_cax, _y+_cy);
			}
			TranslateX(mag_x);
		}else{
			collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
			TranslateX(speed);
		}
	}
	public void VerticleMoving(Man man){
		if(_x<mapR&&man.state.magnetic!=0){
			if(mag_y>0){
				collider=new RectF(_x+_cx, _ay-_cay+mag_y, _ax-_cax, _y+_cy);
			}else{
				collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy+mag_y);
			}
			TranslateY(mag_y);
		}else{
			collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		}
	}
	public void Eated(Man man,GameLogic GmL){
		if(state==0&&CheckCollision(man.collider,this.collider)){
			state++;
			switch(type){
				case 0://����
					man.state.Flying(3*60,GmL);
					break;
				case 1:
					man.state.Magnetic(5*60);
					break;
				case 2:
					man.state.GetShield(-1);//���õĶ�
			}
			
		}
	}
	public void Eated2(Man man,GameLogic2 GmL){
		if(state==0&&CheckCollision(man.collider,this.collider)){
			state++;
			switch(type){
				case 0://����
					man.state.Flying2(3*60,GmL);
					break;
				case 1:
					man.state.Magnetic(5*60);
					break;
				case 2:
					man.state.GetShield(2*60*60);
			}
			
		}
	}
	
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;
		if(state!=0) return ;
		int r=R.drawable.game_boom00;
		switch(type){
			case 0:
				r=R.drawable.game_prop_flying;
				break;
			case 1:
				r=R.drawable.game_prop_magnet;
				break;
			case 2:
				r=R.drawable.game_prop_shield;
		}
		Draw4(gl,_x,_y,_ax,_ay, GmObjZ, r);
	}
}
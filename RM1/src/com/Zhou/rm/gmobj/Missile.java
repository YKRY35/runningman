package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.RectF;
import android.util.Log;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.novice.GameLogic2;

public class Missile extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=100,_h=100;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//��ײ��������ֵ,��ͼƬ������
	private RectF collider;
	private int state;
	private float mySpeed;
	
	public Missile(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		this.state=0;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed){//speed<0
		float extraSpeed=speed;
		if(_x>mapR){
			mySpeed=speed;
		}else mySpeed=speed+extraSpeed;
		collider=new RectF(_x+_cx+mySpeed, _ay-_cay, _ax-_cax, _y+_cy);
		TranslateX(mySpeed);
	}
	
	public void CollideL(Man man){//����ײ
		if(state==0&&CheckCollision(man.collider,this.collider)){//��ײ
			man.GoBackward(_x+_cx,this);
		}
			
	}
	public void ManGBArr(Man man){
	/*	if(man.state.ghostKiller==0&&man.state.ghostKillerDelay==0)
			man.state.MeetGhost();
		else{
			state=1;
		//	SoundPlay();
		}*/
		KillMan(man);
	}
	public void CollideB(Man man){//����ײ
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(state==0&&CheckCollision(man.collider,this.collider)){//��ײ
			man.ForceDown(_y+_cy, this);
		}
			
	}
	public void ManFDArr(Man man){
	/*	if(man.state.ghostKiller==0&&man.state.ghostKillerDelay==0)
			man.state.MeetGhost();
		else{
			state=1;
		//	SoundPlay();
		}*/
		KillMan(man);
	}
	
	public void CollideT(Man man){//�ϱ�����ײ���
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(state==0&&CheckCollision(man.collider,this.collider)){
			man.ForceUp(_ay-_cay, this);
		}
	}
	public void ManFUArr(Man man, GameLogic GmL){
		KillMan(man);
	}
	public void ManFUArr2(Man man, GameLogic2 GmL){
		KillMan(man);
	}
	public void KillMan(Man man){
		if(!developerMode&&man.state.ghostKiller==0&&man.state.ghostKillerDelay==0){
			man.state.dead=true;
		}else if(!developerMode&&man.state.ghostKiller!=0){
			man.state.ghostKiller=0;//�ƶ�
			man.state.ghostKillerDelay=2*60;//�����޵�
		}else{
			state=1;
			man.killGhostCnt++;
			SoundPlay();
		}
	}
	public void SoundPlay(){
		soundPool.play(mscIdMap.get(3), gameVolume, gameVolume, 1, 0, 1.0f);
	}
	
	public void DrawSelf(GL10 gl){
		//����������ʾ 2��/s 1s
		if( (_x>=mapR&&_x<mapR+20*-mySpeed)||(_x>=mapR+40*-mySpeed&&_x<mapR+60*-mySpeed)){
			Draw4(gl, mapL, _y, mapR, _y+25, GmObjZ, R.drawable.game_missile_tip);
		}
		else if(_x<=mapR){
			if(state==0){
				Draw4(gl, _x, _y, _ax, _ay, GmObjZ, R.drawable.game_missile);
			}else{//picH=220 picW=160
				Draw4(gl, _x, _y, _ax, _ay+_h*0.375f, GmObjZ, R.drawable.game_boom00+state-1);
				if(state+1<25) state++;
			}
		}
	}
}
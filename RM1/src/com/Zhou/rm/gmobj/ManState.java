package com.Zhou.rm.gmobj;

import android.util.Log;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.novice.GameLogic2;

import static com.Zhou.rm.Constant.*;

public class ManState{
	
	public ManState(float _r){
		this._r=_r;
		this._v=_vbeg;
		this.show=false;
		this.magnetic=0;
		ResetJmp();
		ResetSqt();
		OtherReset();
	}
	public void Revive(){
		ResetJmp();
		ResetSqt();
		OtherReset();
	}
	public float _r;//向右速度
	
	public float _v;//垂直速度
	public float _a;//加速度
	public static final float _vbeg=-1;//初始垂直速度
	public static final float gra_a=-1.8f;//重力加速度
	public static final float gli_v=-0.95f;//滑翔速度
	public boolean jmp_pressed;
	public int glided;//状态
	public int gliFraCnt;//已滑翔的帧数
	public final int gliFraCntMax=90;//可滑翔
	
	public static final float jmp1=31;//第一次起跳速度
	public static final float jmp2=25;//第二次起跳速度
	public static final float boxU=25;//踩到box上升速度
	public static final float ghostU=25;//踩到ghost上升速度
	public static final float springU=45;//踩到弹簧上升
	
	
	public int jmpCnt;//第几跳
	public static final int jmpCntMax=3;//三连跳
	public int jmpBuff;//缓冲
	public boolean jmpLock;//跳跃锁
	
	public void ResetJmp(){
		jmpCnt=0;
		jmpBuff=0;
		jmpLock=false;
		jmp_pressed=false;
		glided=0;
		gliFraCnt=0;
		_a=gra_a;
	}
	public void UserJmp(){
		if(jmpLock||flying!=0) return ;
		if(jmpBuff<jmpCntMax){
			jmpBuff++;
			jmp_pressed=false;
		}
	}
	public void JmpOnce(){
		jmpBuff=1;
		jmpCnt=1;
	}
	public void UpdateState(Man man){
		UpdateAddition();
		if(man._ax<mapL||man._ay<mapB) dead=true;
		if(developerMode) dead=false;
	/*	if(flying==0){
			if(meetGhost&&ghostKiller!=0){//盾破碎
				ghostKiller=0;
				ghostKillerDelay=2*60;////延时
			}else if(meetGhost&&ghostKiller==0){
				dead=true;
			}
		}
		meetGhost=false;*/
	}
	public void AdjV(Man man){
		_a=gra_a;
		
		if(flying!=0){//正在飞行
			ResetJmp();
		//	_v=0;
			_a=0;
		//	Log.v("man._y",String.valueOf(man._y));
			if(man._y<flyingY-2*flyingDY){//相距太远，迅速就位
				_v=25f;
			}else if(man._y>flying+2*flyingDY){
				_v=-25f;
			}else if(man._y<=flyingY-flyingDY){//应上升
				_v=1f;
			}else if(man._y>flyingY+flyingDY){
				_v=-1f;
			}
			soundPool.play(mscIdMap.get(4), gameVolume, gameVolume, 1, 0, 1.0f);
			return ;
		}
		if(_v<=0&&jmpCnt<jmpBuff){//跳
			if(jmpCnt==0){//一跳
				_v=jmp1;
			}else if(jmpCnt==1){//二跳
				_v=jmp2;
			}else if(jmpCnt==2){
				_v=jmp2;
			}
			
			jmpCnt++;
		}
		if(_v<=0&&jmpCnt==jmpCntMax){
			if(jmp_pressed&&glided<2&&gliFraCnt<gliFraCntMax){
				_v=gli_v;
				glided=1;
				gliFraCnt++;
			}else if(glided==1){
				glided=2;
			}
		}
		
	}
	public boolean OTG,preOTG;//站在地上
	public boolean sqt;//是否蹲着
	public boolean sqt_pressed;//用户是否按着蹲
	public static final float _h=100;
	
	public void ResetSqt(){
		OTG=false;
		sqt=false;
		sqt_pressed=false;
	}
	
	public void OtherReset(){
		ghostKiller=0;
		bldgKiller=0;
		flying=0;
		magnetic=0;
		skill1Cd=0;
		this.dead=false;
		this.deadShow=false;
	}
	public void UpdateAddition(){
		if(ghostKiller>0) ghostKiller--;
		if(ghostKillerDelay>0) ghostKillerDelay--;
		if(bldgKiller>0) bldgKiller--;
		if(flying>0) flying--;
		if(magnetic>0) magnetic--;
		if(skill1Cd>0) skill1Cd--;
	}
	public void Flying(int frameCnt,GameLogic GmL){//飞行帧数
		flying=ghostKiller=bldgKiller=frameCnt;
		GmL.stageSpeedInc=GmL.stageSpeed*2;
		GmL.stageSpeedInc_time=frameCnt;
	}
	
	public void Flying2(int frameCnt,GameLogic2 GmL){//飞行帧数
		flying=ghostKiller=bldgKiller=frameCnt;
		GmL.stageSpeedInc=GmL.stageSpeed*2;
		GmL.stageSpeedInc_time=frameCnt;
	}
	
	public void GetShield(int frameCnt){
		ghostKiller=frameCnt;
	}
	public int flying;//飞
	public int ghostKiller,ghostKillerDelay;//无惧怪物,盾破碎后无敌延时
	public int bldgKiller;//无惧建筑物
	private final float flyingY=0,flyingDY=30;//飞行时应有的纵坐标,波动值
	
	public boolean dead,deadShow;
//	private boolean meetGhost;
/*	public void MeetGhost(){
		meetGhost=true;
	}*/
	public boolean show;//开场动画
	
	public int magnetic;
	public void Magnetic(int frameCnt){
		magnetic=frameCnt;
	}
	public int skill1Cd;
	public final int skill1CdMax=90*60;//90s
	public synchronized void UseSkill1(GameLogic GmL){
		if(skill1Cd==0){
			skill1Cd=skill1CdMax;
			Flying(1*60, GmL);
		}
	}
}
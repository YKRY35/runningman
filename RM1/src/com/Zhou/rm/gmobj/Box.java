package com.Zhou.rm.gmobj;

import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

import com.Zhou.rm.GameLogic;
import com.Zhou.rm.R;
import com.Zhou.rm.Constant.GameObj;
import com.Zhou.rm.novice.GameLogic2;

import android.graphics.RectF;

public class Box extends GameObject{
	float _x,_y,_ax,_ay;
	public final float _w=150,_h=150;
	public final float _cx=0,_cy=0,_cax=0,_cay=0;//碰撞检测坐标差值,向图片内收缩
	private RectF collider;
	private int state;
	
	public Box(float _x,float _y){
		this._x=_x;
		this._y=_y;
		this._ax=_x+_w;
		this._ay=_y+_h;
		this.state=0;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	private void TranslateX(float deltaX){
		this._x+=deltaX;
		this._ax+=deltaX;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	private void TranslateY(float deltaY){
		this._y+=deltaY;
		this._ay+=deltaY;
		super.SendToFather(_x, _y, _ax, _ay);
	}
	
	public void GoAhead(float speed){//speed<0
		collider=new RectF(_x+_cx+speed, _ay-_cay, _ax-_cax, _y+_cy);
		TranslateX(speed);
	}
	
	public boolean CollideA(Man man){
		return state==0&&CheckCollision(man.collider,collider);
	}
	
	public void CollideL(Man man){//必要时，把man往左推
		if(state==0&&CheckCollision(man.collider,this.collider)){//碰撞
			if(man.state.bldgKiller==0)
				man.GoBackward(_x+_cx,this);
			else{
				SoundPlay();
				man.wreckBoxCnt++;
				state=1;
			}
		}
	}
	
	public void CollideT(Man man){//上表面碰撞检测
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(state==0&&CheckCollision(man.collider,this.collider)){
			man.ForceUp(_ay-_cay, this);
		}
	}
	public void ManFUArr(Man man, GameLogic GmL){
		man.state._v=ManState.boxU;
		man.state.JmpOnce();
		state=1;
		man.wreckBoxCnt++;
		SoundPlay();
	}
	public void ManFUArr2(Man man, GameLogic2 GmL){
		man.state._v=ManState.boxU;
		man.state.JmpOnce();
		state=1;
		man.wreckBoxCnt++;
		SoundPlay();
	}
	public void CollideB(Man man){//下表面碰撞检测
		collider=new RectF(_x+_cx, _ay-_cay, _ax-_cax, _y+_cy);
		if(state==0&&CheckCollision(man.collider,this.collider)){
			man.ForceDown(_y+_cy, this);
		}
	}
	public void ManFDArr(Man man){//下表面碰撞
		man.state._v=-man.state._v;//相反速度
		state=1;
		man.wreckBoxCnt++;
		SoundPlay();
	}
	private void SoundPlay(){
		soundPool.play(mscIdMap.get(3), gameVolume, gameVolume, 1, 0, 1.0f);
	}
	
	
	final float zNear=-1.09f;
	final float zFar=2*GmObjZ-zNear;
	public void DrawSelf(GL10 gl){
		if(_x>mapR) return ;//屏幕外
		if(state==0){
			Draw4(gl, _x, _y, _ax, _ay, zNear, R.drawable.game_box_surface);
			Draw42(gl, _x, zNear, _ax, zFar, _y, R.drawable.game_box_surface);
			Draw42(gl, _x, zNear, _ax, zFar, _ay, R.drawable.game_box_surface);
			Draw43(gl, _y, zFar, _ay, zNear, _x, R.drawable.game_box_surface);
			Draw43(gl, _y, zNear, _ay, zFar, _ax, R.drawable.game_box_surface);
		}else{//picH=220 picW=160
			Draw4(gl, _x, _y, _ax, _ay+_h*0.375f, GmObjZ, R.drawable.game_boom00+state-1);
			if(state+1<25) state++;
		}
	}
	
}
package com.Zhou.rm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

public class ExitDialog implements DialogInterface.OnClickListener{
	
	public ExitDialog(Activity context){
		AlertDialog.Builder builder=new AlertDialog.Builder(context);
		builder.setTitle("退出游戏\n确定吗？");
		builder.setIcon(R.drawable.exit_thinking);
		builder.setPositiveButton("是",this);
		builder.setNegativeButton("否",this);
		builder.create().show();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch(which){
			case Dialog.BUTTON_POSITIVE:
				System.exit(0);
				break;
			case Dialog.BUTTON_NEGATIVE:
				
				break;
		}
	}
	
}
package com.Zhou.rm.novice;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.MotionEvent;
import static com.Zhou.rm.Constant.*;

import com.Zhou.rm.GameBorder;
import com.Zhou.rm.LoadUtil;
import com.Zhou.rm.MainActivity;
import com.Zhou.rm.R;
import com.Zhou.rm.gmobj.GameObject;

public class GameView2 extends GLSurfaceView{

	GameLogic2 GmL;
	private GL10Button2 jmpBtn,sqtBtn,pauseBtn;
	private GL10Button2 exitBtn,backBtn,againBtn;
	private List<GL10Button2> buttons=new ArrayList<GL10Button2>();
	private GameBorder border;
	private boolean isInit=false;
	public boolean gamePause=false,gameOver=false;
	private MainActivity activity;
	public GameView2(Context context) {
		super(context);
		this.activity=(MainActivity) context;
		InitButtons();
		Reset();//不能确定：在draw前实例化gameLogic.man
		border=new GameBorder();
		setRenderer(new MRenderer());
	}
	public void Reset(){
		isInit=false;//锁住屏幕，不能点击
		GmL=GameLogic2.GetInstance();
		GmL.Reset();//逻辑初始化
		gamePause=false;
		gameOver=false;
		for(GL10Button2 gb:buttons){
			gb.Reset();
		}
		suc=false;
	}
	private void InitButtons(){
		jmpBtn=new GL10Button2(1600, 800, 1910, 1080, -1, R.drawable.game_jmp_btn85, R.drawable.game_jmp_btn55, ShowType.always);
		buttons.add(jmpBtn);
		sqtBtn=new GL10Button2(10, 800, 320, 1080, -1, R.drawable.game_sqt_btn85, R.drawable.game_sqt_btn55, ShowType.always);
		buttons.add(sqtBtn);
		
		pauseBtn=new GL10Button2(1720, 0, 1920, 100, -1, R.drawable.game_pause, ShowType.always);
		buttons.add(pauseBtn);

		againBtn=new GL10Button2(600,500,900,800,-1,R.drawable.game_button_gotomainview, ShowType.over);//再来一局
		buttons.add(againBtn);
		exitBtn=new GL10Button2(600,500,900,800,-1,R.drawable.game_button_gotomainview, ShowType.not_over__and_pause);
		buttons.add(exitBtn);
		backBtn=new GL10Button2(1000,500,1300,800,-1,R.drawable.game_button_backtogame, ShowType.not_over__and_pause);
		buttons.add(backBtn);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		if(!isInit) return false;
		int pointerId=GetPointerId(event.getAction());//第几根手指 会变
		int fingerId=event.getPointerId(pointerId);//手指索引 跟随手指不变
		float mx=event.getX(pointerId);
		float my=event.getY(pointerId);
		switch(event.getActionMasked()){
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				if(!gamePause&&GmL.man.state.show&&!GmL.man.state.dead){
					if(sqtBtn.Pressed(mx, my, fingerId, BtnType2.sqt)){//蹲
						GmL.man.state.sqt_pressed=true;
					}else if(jmpBtn.Pressed(mx, my, fingerId, BtnType2.jmp)){
						GmL.man.state.jmp_pressed=true;
						GmL.man.state.UserJmp();
						if(GmL.man.state.preOTG){
							GmL.man.state.sqt_pressed=false;
							sqtBtn.fingerId=-1;
						}
					}
				}
				if(gameOver){
					againBtn.Pressed(mx, my, fingerId, BtnType2.again);
				}
				if(gamePause&&!gameOver){
					exitBtn.Pressed(mx, my, fingerId, BtnType2.exit);
					backBtn.Pressed(mx, my, fingerId, BtnType2.back);
				}
				if(!gamePause&&pauseBtn.Pressed(mx, my, fingerId, BtnType2.pause)){
					gamePause=true;
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				BtnType2 btnType=BtnType2.nothing;
				for(GL10Button2 gb:buttons){
					btnType=gb.Release(fingerId,mx,my);
					if(btnType!=BtnType2.nothing) break;
				}
				switch(btnType){
					case sqt:
						GmL.man.state.sqt_pressed=false;
						break;
					case jmp:
						GmL.man.state.jmp_pressed=false;
						break;
					case exit:
						activity.handler.sendEmptyMessage(ViewId.MenuView);
						break;
					case back:
						gamePause=false;
						break;
					case revive:
						GmL.Revive();
						gameOver=false;
						gamePause=false;
						break;
					case again:
						activity.handler.sendEmptyMessage(ViewId.NoviceScoreView);
						break;
					default:
						break;
				}
				break;
		}
		return true;
	}
	private int GetPointerId(int action){
    	return (action&0xff00)>>8;
    }
	boolean suc;
	class MRenderer implements Renderer{
		
		@Override
		public void onDrawFrame(GL10 gl) {
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT);
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
			
		//	if(GmL.reset_flag) GmL.Reset();
		//	long startTime=System.currentTimeMillis();
			if(GmL.suc&&!suc){
				activity.handler.sendEmptyMessage(ViewId.NoviceScoreView);
				suc=true;//执行一次
				gamePause=true;
			}
			if(!gamePause)
				GmL.CalcFrame(GameView2.this);
			Draw(gl);
		//	Log.v("CalcTime",String.valueOf(System.currentTimeMillis()-startTime));
		}
		
		public void Draw(GL10 gl){//顺序影响blend混合结果，可能有bug
			//画背景
			isInit=true;

		//	long startTime=System.currentTimeMillis();
			
			Draw4(gl,mapL*2,mapB*2,mapR*2,mapT*2,-2,R.drawable.game_bg1);//暂定最大深度为2

			
			for(GameObject go:GmL.objects){
				go.DrawSelf(gl);
			}
			
		//	Log.v("CalcTime",String.valueOf(System.currentTimeMillis()-startTime));
			
			for(GameObject go:GmL.foods){
				go.DrawSelf(gl);
			}
			GmL.man.DrawSelf(gl, gamePause);
			
			border.DrawSelf(gl);
			for(GL10Button2 gb:buttons){
				gb.DrawSelf(gl,gamePause,gameOver);
			}
			
			
		}
		
		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {
			gl.glViewport(0, 0, width, height);
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
		//	float ratio=(float)width/height;
		//	gl.glFrustumf(-ratio, ratio, -1, 1, 1, 100);
			gl.glFrustumf(-1, 1, -1, 1, 1, 100);
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			gl.glEnable(GL10.GL_DEPTH_TEST);
			gl.glClearDepthf(1.0f);
			gl.glDepthFunc(GL10.GL_LESS);
			gl.glEnable(GL10.GL_BLEND);
			gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
			gl.glClearColor(1, 0, 0, 1);
			
			LoadTexture(gl);
		}
		
		void LoadTexture(GL10 gl){
			gl.glEnable(GL10.GL_TEXTURE_2D);
			gl.glGenTextures(texSize, textures, 0);
			for(int i=0;i<texSize;i++){
				gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[i]);
				int rr=fistResId+i;
				addTex(i,rr);
				Bitmap bm=LoadUtil.MyDecodeStream(rr);
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bm, 0);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
				bm.recycle();
			}
		}
	}
}
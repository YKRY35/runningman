package com.Zhou.rm.novice;

import static com.Zhou.rm.Constant.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Align;
import android.os.Environment;
import android.text.format.Time;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.Zhou.rm.LoadUtil;
import com.Zhou.rm.MainActivity;
import com.Zhou.rm.MyButton;
import com.Zhou.rm.R;
import com.Zhou.rm.Constant.ViewId;

public class ScoreView2 extends View{

	public ScoreView2(Context context) {
		super(context);
		this.activity=(MainActivity) context;
		
	}
	MainActivity activity;
	boolean isInit=false;
	Bitmap[] bitmaps=new Bitmap[10];
	Bitmap sucPic,faiPic;
	Paint txtPaint;
	
	MyButton back,save;
	List<MyButton> buttons=new ArrayList<MyButton>();
	boolean saveLock;
	float premx,premy;
	final float hand_shake=25;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!isInit) return false;
		float mx=event.getX();
		float my=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				for(MyButton b:buttons){
					b.Pressed(mx, my);
				}
				premx=mx;
				premy=my;
				break;
			case MotionEvent.ACTION_UP:
				if(Math.abs(mx-premx)<hand_shake&&Math.abs(my-premy)<hand_shake){
					if(back.Release(mx, my)){//返回
						activity.handler.sendEmptyMessage(ViewId.MenuView);
					}else if(!saveLock&&save.Release(mx, my)){
						saveLock=true;
						SaveBitmap();
					}
				}
				break;
		}
		
		return true;
	}
	
	long moneyCnt,meters,wreckBoxCnt,killGhostCnt;
	long score;
	boolean suc;
	boolean firstSuc;
	public void Reset(){
		GameLogic2 GmL=GameLogic2.GetInstance();
		moneyCnt=GmL.man.moneyCnt;
		meters=(long) GmL.meters/6;
		wreckBoxCnt=GmL.man.wreckBoxCnt;
		killGhostCnt=GmL.man.killGhostCnt;
		score=moneyCnt*60+meters+wreckBoxCnt*90+killGhostCnt*100;
		suc=GmL.suc;
		
		SharedPreferences sp=activity.getSharedPreferences("novice", Context.MODE_PRIVATE);
		firstSuc=sp.getBoolean("novice", false);
		if(!firstSuc&&suc){//第一次过关
			firstSuc=true;
			GetDiamondsCnt(activity,20);
			diamondsCnt+=20;
			Editor editor=sp.edit();
			editor.putBoolean("novice", true);
			editor.commit();
		}else if(firstSuc){
			firstSuc=false;
		}
		
		saveLock=false;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(!isInit){
			for(int i=0;i<10;i++){
				bitmaps[i]=LoadUtil.MyDecodeStream(R.drawable.ui_num0+i);
			}
			
			back=new MyButton(0,0,200,200,R.drawable.ui_button_back);
			buttons.add(back);
			save=new MyButton(400,0,700,200,R.drawable.ui_screen_shot);
			buttons.add(save);
			
			sucPic=LoadUtil.MyDecodeStream(R.drawable.ui_novice_success);
			faiPic=LoadUtil.MyDecodeStream(R.drawable.ui_novice_failed);
			
			txtPaint=new Paint();
			
			isInit=true;
		}
		Draw(canvas);
		
		postInvalidate();
	}
	public void SaveBitmap(){
		Bitmap myScreen=Bitmap.createBitmap(SW, SH, Config.ARGB_8888);
		Canvas canvas=new Canvas(myScreen);
		Draw(canvas);
		
		Time t=new Time();
		t.setToNow();
		String fileName="r";
		fileName+=String.valueOf(t.year);
		if(t.month+1<10) fileName+="0";
		fileName+=String.valueOf(t.month+1);
		if(t.monthDay<10) fileName+="0";
		fileName+=String.valueOf(t.monthDay);
		if(t.hour<10) fileName+="0";
		fileName+=String.valueOf(t.hour);
		if(t.minute<10) fileName+="0";
		fileName+=String.valueOf(t.minute);
		if(t.second<10) fileName+="0";
		fileName+=String.valueOf(t.second);
		fileName+=".png";
	//	Log.v("fileName",fileName);
		
		File sd=Environment.getExternalStorageDirectory();
		String folderName="/com.Zhou.rm/ScreenShot"; 
		String path=sd.getPath()+folderName; 
		File folder=new File(path);
		boolean suc=true;
		if(!folder.exists()) suc=folder.mkdirs();
		if(!suc){//不存在且创建文件夹失败
			ToastMsg("保存失败");
			saveLock=false;
			return ;
		}
	//	Log.v("suc",String.valueOf(suc));
		File file=new File(path,fileName);
		try {
			FileOutputStream out=new FileOutputStream(file);
			myScreen.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.flush();
			out.close();
			ToastMsg("截图已保存为SD卡下"+"/com.Zhou.rm/ScreenShot/"+fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			ToastMsg("保存失败");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ToastMsg("保存失败");
			e.printStackTrace();
		}
		
		saveLock=false;
	}
	public void ToastMsg(String msg){
		Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
	}
	
	final String jinbishu="金币数：",defen="得分：",juli="距离：";
	private void Draw(Canvas canvas){
		synchronized(canvas){
			canvas.drawBitmap(ui_background2, null, screenRect, null);
			for(MyButton mb:buttons){
				mb.DrawSelf(canvas);
			}
			
			txtPaint.setTextSize(160f*SH/1920);
			txtPaint.setTextAlign(Align.RIGHT);
			txtPaint.setColor(Color.WHITE);
			
			MyDrawText(jinbishu, 900, 450, txtPaint, canvas);//金币数
			DrawNums(moneyCnt, 1000, 360, 100, 100, canvas);
			
			MyDrawText(juli, 900, 600, txtPaint, canvas);//距离
			DrawNums(meters, 1000, 510, 100, 100, canvas);
			
			MyDrawText(defen, 900, 750, txtPaint, canvas);//得分
			DrawNums(score, 1000, 660, 100, 100, canvas);
			
			if(firstSuc){
				txtPaint.setTextAlign(Align.CENTER);
				MyDrawText("第一次完成新手训练，赠送您20钻石", 920, 900, txtPaint, canvas);
			}
			
			if(suc){
				canvas.drawBitmap(sucPic, null,
						new Rect(FitScreenX(1450),FitScreenY(100),FitScreenX(1750),FitScreenY(400)), null);
			}else{
				canvas.drawBitmap(faiPic, null,
						new Rect(FitScreenX(1450),FitScreenY(100),FitScreenX(1750),FitScreenY(400)), null);
			}
			
		}
	}
	
	private void MyDrawText(String s, int x, int y, Paint paint, Canvas canvas){
		x=FitScreenX(x);
		y=FitScreenY(y);
		canvas.drawText(s, x, y, paint);
	}
	
	private void DrawNums(long num,int dx,int dy,int zw,int zh,Canvas canvas){//android坐标  左上
		String snum=String.valueOf(num);
		dx=FitScreenX(dx);
		dy=FitScreenY(dy);
		zw=FitScreenX(zw);
		zh=FitScreenY(zh);
		for(int i=0;i<snum.length();i++){
			int inum=Integer.valueOf(snum.substring(i, i+1));
			canvas.drawBitmap(bitmaps[inum], null, new Rect(dx+i*zw,dy,dx+(i+1)*zw,dy+zh), null);
		}
	}
}
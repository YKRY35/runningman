package com.Zhou.rm.novice;

import static com.Zhou.rm.Constant.Draw4;
import static com.Zhou.rm.Constant.*;

import javax.microedition.khronos.opengles.GL10;

enum BtnType2{
	jmp,sqt,skill1,restart,faster,slower,reset_man,pause,revive,exit,back,again,nothing
};
enum ShowType{//什么时候画出
	always,developer,pause,not_over__and_pause,over
}
public class GL10Button2{//android坐标系
	float _x,_y,_ax,_ay;
	float _z;
	public int fingerId;
	private BtnType2 btnType;
	private int resId,resId_pressed;
	ShowType showType;
	public GL10Button2(float _x,float _y,float _ax,float _ay,float _z,int resId,ShowType showType){//统一生成纹理
		this._x=_x*SW/1920;
		this._y=_y*SH/1080;
		this._ax=_ax*SW/1920;
		this._ay=_ay*SH/1080;//适应手机
		this._z=_z;
		this.resId=resId;
		this.resId_pressed=-1;
		this.fingerId=-1;
		this.showType=showType;
	}
	public GL10Button2(float _x,float _y,float _ax,float _ay,float _z,int resId,int resId_pressed,ShowType showType){
		this._x=_x*SW/1920;
		this._y=_y*SH/1080;
		this._ax=_ax*SW/1920;
		this._ay=_ay*SH/1080;//适应手机
		this._z=_z;
		this.resId=resId;
		this.resId_pressed=resId_pressed;
		this.fingerId=-1;
	}
	public void Reset(){
		fingerId=-1;
	}
	public boolean Pressed(float mx, float my, int fingerId,BtnType2 btnType){
		if(this.fingerId==-1&&mx>=_x&&mx<=_ax&&my>=_y&&my<=_ay){
			this.fingerId=fingerId;
			this.btnType=btnType;
			
			return true;
		}
		return false;
	}
	public BtnType2 Release(int fingerId,float mx,float my){
		if(this.fingerId==fingerId){
			this.fingerId=-1;
			if(btnType==BtnType2.jmp||btnType==BtnType2.sqt){
				return btnType;
			}else if(mx>=_x&&mx<=_ax&&my>=_y&&my<=_ay){
				return btnType;
			}
		}
		return BtnType2.nothing;
	}
	
	public void DrawSelf(GL10 gl,boolean gamePause,boolean gameOver){
		if(resId==-1) return ;
		if(showType==ShowType.developer&&!developerMode) return ;
		if(showType==ShowType.pause&&!gamePause) return ;
		if(showType==ShowType.over&&!gameOver) return ;
		if(showType==ShowType.not_over__and_pause&&(gameOver||!gamePause)) return ;
		int drawId=resId;
		if(fingerId!=-1&&resId_pressed!=-1) drawId=resId_pressed;
		Draw4(gl, AndXToLogicX(_x), AndYToLogicY(_ay), AndXToLogicX(_ax), AndYToLogicY(_y), _z, drawId);
	}
}
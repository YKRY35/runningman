package com.Zhou.rm.SetV;

import java.util.ArrayList;
import java.util.List;

import com.Zhou.rm.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.widget.Toast;
import static com.Zhou.rm.Constant.*;
import static com.Zhou.rm.LoadUtil.*;

public class DeveloperView extends MySetV{
	public DeveloperView(Context context) {
		this.context=context;
	}
	public void Recycle(){
		point=null;
		points=null;
		if(!point_normal.isRecycled()) point_normal.recycle();
		if(!point_pressed.isRecycled()) point_pressed.recycle();
		if(!point_error.isRecycled()) point_error.recycle();
		linePaint=null;
	}
	
	public void Draw(Canvas canvas) {
		if(!isInit){
			Init();
			isInit=true;
		}
		DrawMPoints(canvas);
		DrawLines(canvas);
	}
	private void DrawMPoints(Canvas canvas){
		for(int i=0;i<MPoint.cnt;i++)
			for(int j=0;j<MPoint.cnt;j++){
				canvas.drawBitmap(point[i][j].GetBitmap(), null, point[i][j].rect, null);
			}
	}
	private void DrawLines(Canvas canvas){
		for(int i=0;i+1<points.size();i++){
			int li=point[0][0].IdToX(points.get(i));
			int ri=point[0][0].IdToY(points.get(i));//li行ri列
			int li1=point[0][0].IdToX(points.get(i+1));
			int ri1=point[0][0].IdToY(points.get(i+1));//li1行ri1列
			DrawLine(point[li][ri]._x,point[li][ri]._y,point[li1][ri1]._x,point[li1][ri1]._y,canvas);
		}
		if(!drawing) return ;
		int ll=point[0][0].IdToX(points.get(points.size()-1));
		int rl=point[0][0].IdToY(points.get(points.size()-1));//li行ri列
		DrawLine(point[ll][rl]._x,point[ll][rl]._y,(int)movingX,(int)movingY,canvas);
	}
	private void DrawLine(int x1,int y1,int x2,int y2,Canvas canvas){
		if(error)linePaint.setColor(Color.RED);
		else linePaint.setColor(Color.BLUE);
		canvas.drawLine(x1, y1, x2, y2, linePaint);
	}
	
	private boolean drawing,error;
	private float movingX,movingY;
	public boolean onTouchEvent(MotionEvent event) {
		if(!isInit) return false;
		movingX=event.getX();
		movingY=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				
				clsPoint();
				drawing=Press(movingX,movingY);
				break;
			case MotionEvent.ACTION_MOVE:
				if(drawing){
					Press(movingX,movingY);
				}
				break;
			case MotionEvent.ACTION_UP:
				if(drawing){
					Check();
				}
				drawing=false;
				break;
		}
		
		
		return true;
	}
	private void clsPoint(){
		for(int i=0;i<MPoint.cnt;i++)
			for(int j=0;j<MPoint.cnt;j++){
				point[i][j].state=MPoint.state_normal;
			}
		points.clear();
		error=false;
	}
	private boolean Press(float x,float y){
		for(int i=0;i<MPoint.cnt;i++)
			for(int j=0;j<MPoint.cnt;j++){
				if(point[i][j].state==MPoint.state_normal&&point[i][j].IsInRect(x, y)){
					point[i][j].state=MPoint.state_pressed;
					points.add(point[i][j].XYToId(i, j));
					return true;
				}
			}
		return false;
	}
	private void Check(){
		String s="";
		for(int i=0;i<points.size();i++)
			s+=points.get(i);
		if(passwdIn.equals(s)){
			developerMode=true;
			Toast.makeText(context, successTipIn, Toast.LENGTH_LONG).show();
		}else if(passwdOut.equals(s)){
				developerMode=false;
				Toast.makeText(context, successTipOut, Toast.LENGTH_LONG).show();
		}else{
			for(int i=0;i<points.size();i++){
				int li=point[0][0].IdToX(points.get(i));
				int ri=point[0][0].IdToY(points.get(i));//li行ri列
				point[li][ri].state=MPoint.state_error;
				error=true;
			}
		}
	}
	private void Init(){
		
		int a=Math.min(SW, SH);
		point=new MPoint[MPoint.cnt][MPoint.cnt];
		int delta=a/(MPoint.cnt+1);
		int r=a/12;
		for(int i=0;i<MPoint.cnt;i++){
			int ty=(SH-a)/2+delta*(i+1);
			for(int j=0;j<MPoint.cnt;j++){
				int tx=(SW-a)/2+delta*(j+1);
				point[i][j]=new MPoint(tx,ty,tx-r,ty-r,tx+r,ty+r,MPoint.state_normal);
			}
		}
		points.clear();
		point_normal=MyDecodeStream(R.drawable.ui_point_normal);
		point_pressed=MyDecodeStream(R.drawable.ui_point_pressed);
		point_error=MyDecodeStream(R.drawable.ui_point_error);
		linePaint=new Paint();
		linePaint.setAntiAlias(true);
		linePaint.setStrokeWidth(r/10);
		
		drawing=false;
		error=false;
		
	}

	private Context context;
	private boolean isInit=false;
	
	private MPoint[][] point;
	class MPoint{
		public int _x,_y;
		public int _l,_t,_r,_b;
		public Rect rect;
		public int state;
		public MPoint(int _x,int _y,int _l,int _t,int _r,int _b,int state){
			this._x=_x;
			this._y=_y;
			this._l=_l;
			this._t=_t;
			this._r=_r;
			this._b=_b;
			this.rect=new Rect(_l,_t,_r,_b);
			this.state=state;
		}
		public boolean IsInRect(float x,float y){
			return (x>=_l&&x<=_r&&y<=_b&&y>=_t);
		}
		public Bitmap GetBitmap(){
			if(state==state_normal) return point_normal;
			else if(state==state_pressed) return point_pressed;
			else return point_error;
		}
		public int IdToX(int id){
			return id/cnt;
		}
		public int IdToY(int id){
			return id%cnt;
		}
		public int XYToId(int x,int y){
			return x*cnt+y;
		}
		public static final int state_normal=1;
		public static final int state_pressed=2;
		public static final int state_error=3;
		public static final int cnt=3;
	}
	private List<Integer> points=new ArrayList<Integer>();
	private Bitmap point_normal,point_pressed,point_error;
	private Paint linePaint;
	private final String passwdIn="042",passwdOut="648";
	private final String successTipIn="成功进入开发者模式",successTipOut="成功退出开发者模式";
}
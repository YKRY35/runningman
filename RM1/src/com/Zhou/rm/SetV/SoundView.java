package com.Zhou.rm.SetV;

import java.util.ArrayList;
import java.util.List;

import com.Zhou.rm.LoadUtil;
import com.Zhou.rm.MyButton;
import com.Zhou.rm.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import static com.Zhou.rm.Constant.*;

public class SoundView extends MySetV{
	boolean isInit=false;
	MyButton bgmS,buttonMscS,gameMscS;
	List<MyButton> buttons=new ArrayList<MyButton>();
	Bitmap tickPic;
	
	Context context;
	public SoundView(Context context){
		this.context=context;
	}
	
	float premx,premy;
	private final float hand_shake=10;//���ֶ�
	public boolean onTouchEvent(MotionEvent event) {
		if(!isInit) return false;
		float mx=event.getX(),my=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				premx=mx;
				premy=my;
				for(MyButton mb:buttons)
					mb.Pressed(mx, my);
				break;
			case MotionEvent.ACTION_UP:
				if(Math.abs(mx-premx)<hand_shake&&Math.abs(my-premy)<hand_shake){
					if(bgmS.Release(mx, my)){//��������
						bgmSound=!bgmSound;
						UpdatePreferences("bgmSound",bgmSound);
						if(bgmSound) bgmVolume=bgmVolumeMax;
						else bgmVolume=0;
						bgm.setVolume(bgmVolume, bgmVolume);
					}else if(buttonMscS.Release(mx, my)){
						buttonSound=!buttonSound;
						UpdatePreferences("buttonSound",buttonSound);
						if(buttonSound) buttonVolume=buttonVolumeMax;
						else buttonVolume=0;
					}else if(gameMscS.Release(mx, my)){//��Ϸ����
						gameSound=!gameSound;
						UpdatePreferences("gameSound",gameSound);
						if(gameSound) gameVolume=gameVolumeMax;
						else gameVolume=0;
					}
					
				}
				break;
		}
		
		return true;
	}
	public void UpdatePreferences(String keyName,boolean value){
		SharedPreferences sp=context.getSharedPreferences("Sound", Context.MODE_PRIVATE);
		Editor editor=sp.edit();
		editor.putBoolean(keyName, value);
		editor.commit();
	}
	
	public void Draw(Canvas canvas) {
		if(!isInit){
			bgmS=new MyButton(800,230,1300,400,R.drawable.ui_setting_bgm);
			buttons.add(bgmS);
			buttonMscS=new MyButton(800,450,1300,620,R.drawable.ui_setting_button_sound);
			buttons.add(buttonMscS);
			gameMscS=new MyButton(800,670,1300,840,R.drawable.ui_setting_game_sound);
			buttons.add(gameMscS);
			
			tickPic=LoadUtil.MyDecodeStream(R.drawable.ui_setting_tick, FitScreenX(180), FitScreenY(170));
			
			isInit=true;
		}
		
		DrawButtons(canvas);
		DrawTick(canvas);
	}
	
	public void DrawButtons(Canvas canvas){
		for(MyButton mb:buttons){
			mb.DrawSelf(canvas);
		}
	}
	
	public void DrawTick(Canvas canvas){
		if(bgmSound)
			MyDrawBitmap(canvas,new Rect(1120,230,1300,400),tickPic);
		if(buttonSound)
			MyDrawBitmap(canvas,new Rect(1120,450,1300,620),tickPic);
		if(gameSound)
			MyDrawBitmap(canvas,new Rect(1120,670,1300,840),tickPic);
	}
	
	public void MyDrawBitmap(Canvas canvas,Rect r,Bitmap bm){
		r.left=FitScreenX(r.left);
		r.top=FitScreenY(r.top);
		r.right=FitScreenX(r.right);
		r.bottom=FitScreenY(r.bottom);
		canvas.drawBitmap(bm, null, r, null);
	}
	
}
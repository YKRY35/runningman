package com.Zhou.rm.SetV;

import com.Zhou.rm.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.view.MotionEvent;
import static com.Zhou.rm.Constant.*;

public class IntroView extends MySetV{
	Context context;
	boolean isInit=false;
	class Text{
		public int h,h0,baseY,baseY_pre,endY=-10000000;
		public Rect bounds,wordsRect;
		public Paint paint;
	}
	private Text text;
	
	private String str;
	private String zhao="曌";
	
	public IntroView(Context context){
		this.context=context;
		SetString(0);
	}
	public void SetString(int type){
		if(type==0){//规则
			str=context.getResources().getString(R.string.rule);
		}else{//关于
			str=context.getResources().getString(R.string.about);
		}
	}
	private float oldY;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x=event.getX();
		float y=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
					text.baseY_pre=text.baseY;
					oldY=y;
				break;
			case MotionEvent.ACTION_MOVE:
				text.baseY=(int)(text.baseY_pre+y-oldY);
				break;
			case MotionEvent.ACTION_UP:
				break;
		}
		return true;
	}
	
	public void Draw(Canvas canvas) {
		if(!isInit){
			Init();
			isInit=true;
		}
		Calc();
		DrawText(canvas);
	}
	private void Init(){
		
		text=new Text();
		text.wordsRect=new Rect(FitScreenX(400),FitScreenY(200),FitScreenX(1700),FitScreenY(900));
		text.paint=new Paint();
		text.paint.setTextSize(FitScreenX(50));
		text.paint.setTextAlign(Align.LEFT);
		FontMetrics fm=text.paint.getFontMetrics();
		text.h=(int) Math.ceil(fm.descent-fm.ascent);//一个字的高度
		text.h0=(int) Math.ceil(fm.bottom-fm.descent+2);
		text.baseY=text.wordsRect.top;
		text.baseY+=text.h;
	}
	private void DrawText(Canvas canvas){
		canvas.save();
		canvas.clipRect(text.wordsRect);
		
		int wid2=(int) text.paint.measureText("空格");
		int x=text.wordsRect.left,y=text.baseY;
		x+=wid2;
		int len;
		len=str.length();
		for(int i=0;i<len;i++){
			char ch;
			ch=str.charAt(i);
			String chS=String.valueOf(ch);
			if(chS.equals(zhao)){
				x=text.wordsRect.left+wid2;
				y+=text.h;
				continue;
			}
			int wid=(int) text.paint.measureText(chS);
			if(x+wid<=text.wordsRect.right){
				
				canvas.drawText(chS,x,y,text.paint);
				x+=wid;
			}else{
				x=text.wordsRect.left;
				y+=text.h;
				i--;
			}
			
		}
		text.endY=y;
		
		canvas.restore();
	}
	private void Calc(){
		if(text.endY==-10000000) return ;
		if(text.baseY-text.h>text.wordsRect.top){
			int speed=text.baseY-text.h-text.wordsRect.top;
			speed/=8;
			if(speed<=0) speed=1;
			text.baseY-=speed;
		}else if(text.baseY-text.h<text.wordsRect.top&&text.endY<text.wordsRect.bottom-text.h0){
			int dis=Math.min(text.wordsRect.top-(text.baseY-text.h),text.wordsRect.bottom-text.h0-text.endY);
			int speed=dis/8;
			if(speed<=0) speed=1;
			text.baseY+=speed;
		}
	}
	
}
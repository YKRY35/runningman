package com.Zhou.rm;

import java.util.ArrayList;
import java.util.List;

import static com.Zhou.rm.Constant.*;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MenuView extends View{
	MainActivity activity;
	boolean isInit=false;
	final int tho=1000;
	MyButton start,setting,online,novice,exit;
	List<MyButton> buttons=new ArrayList<MyButton>();
	Bitmap loadingPic,blackPic,diamondsPic;
	Paint textPaint;
	public MenuView(Context context) {
		super(context);
		activity=(MainActivity) context;
	}
	public void Reset(){
		for(MyButton b:buttons){
			b.Reset();
		}
		loadingRotate=0;
	}
	
	private final float hand_shake=25;//防手抖
	private float premx,premy;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!isInit||!loadingFinish) return false;
		float mx=event.getX();
		float my=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				for(MyButton b:buttons){
					b.Pressed(mx, my);
				}
				premx=mx;
				premy=my;
				break;
			case MotionEvent.ACTION_UP:
				if(Math.abs(mx-premx)<hand_shake&&Math.abs(my-premy)<hand_shake){
					if(start.Release(mx, my)){//开始
						activity.handler.sendEmptyMessage(ViewId.SelectView);
					}else if(setting.Release(mx, my)){
						activity.handler.sendEmptyMessage(ViewId.SettingView);
					}else if(novice.Release(mx, my)){
						activity.handler.sendEmptyMessage(ViewId.NoviceGameView);
					}else if(exit.Release(mx, my)){
						new ExitDialog(activity);
					}else if(online.Release(mx, my)){
						DisplayToast("下个版本将会开放\n敬请期待",activity);
					}
				}
				break;
		}
		
		return true;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(!isInit){
			start=new MyButton(50,50,1000,1000,R.drawable.start);
			novice=new MyButton(1000,50,1600,500,R.drawable.ui_play_novice);
			online=new MyButton(1000,500,1600,1000,R.drawable.ui_play_online);
			setting=new MyButton(1600,50,1900,350,R.drawable.ui_setting);
			exit=new MyButton(1600,700,1900,1000,R.drawable.ui_exit);
			buttons.add(start);
			buttons.add(novice);
			buttons.add(online);
			buttons.add(setting);
			buttons.add(exit);
			
			textPaint=new Paint();
			textPaint.setTextAlign(Align.CENTER);
			textPaint.setColor(Color.WHITE);
			textPaint.setTextSize(FitScreenY(60));
			textPaint.setStrokeWidth(FitScreenX(30));
			
			loadingPic=LoadUtil.MyDecodeStream(R.drawable.ui_loading2,FitScreenX(600),FitScreenY(600));
			blackPic=LoadUtil.MyDecodeStream(R.drawable.ui_black75);
			diamondsPic=LoadUtil.MyDecodeStream(R.drawable.ui_diamonds,FitScreenX(300),FitScreenY(300));
			
			isInit=true;
		}
		Draw(canvas);
		
		postInvalidate();
	}
	
	int loadingRotate;
	public void Draw(Canvas canvas){
		canvas.drawBitmap(ui_background2, null, screenRect, null);
		for(MyButton mb:buttons)
			mb.DrawSelf(canvas);
		canvas.drawBitmap(diamondsPic, null,
				new Rect(FitScreenX(1600),FitScreenY(350),FitScreenX(1900),FitScreenY(700)), null);
		canvas.drawText(String.valueOf(diamondsCnt), FitScreenX(1750), FitScreenY(650), textPaint);
		
		
		if(!loadingFinish){
			canvas.drawBitmap(blackPic, null, screenRect, null);
			canvas.save();
			canvas.rotate(loadingRotate, SW/2, SH/2);
			canvas.drawBitmap(loadingPic, null, 
					new Rect(FitScreenX(700),FitScreenY(200),FitScreenX(1220),FitScreenY(800)), null);
			loadingRotate++;
			canvas.restore();
		}
	}
	
}
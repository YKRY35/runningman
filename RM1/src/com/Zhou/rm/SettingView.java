package com.Zhou.rm;

import java.util.ArrayList;
import java.util.List;

import com.Zhou.rm.Constant.ViewId;
import com.Zhou.rm.SetV.IntroView;
import com.Zhou.rm.SetV.MySetV;
import com.Zhou.rm.SetV.DeveloperView;
import com.Zhou.rm.SetV.SoundView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import static com.Zhou.rm.Constant.*;

public class SettingView extends View{
	boolean isInit=false;
	MyButton back,soundS,developerS,aboutS,ruleS;
	List<MyButton> buttons=new ArrayList<MyButton>();
	MainActivity activity;
	MySetV currView;
	DeveloperView devV;
	SoundView soundV;
	IntroView introV;
	
	Context context;
	public SettingView(Context context) {
		super(context);
		this.context=context;
		this.activity=(MainActivity) context;
	}
	public void Reset(){
		if(back!=null) back.Reset();
		
	}
	
	float premx,premy;
	final float hand_shake=10;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(!isInit) return false;
		currView.onTouchEvent(event);
		float mx=event.getX();
		float my=event.getY();
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				for(MyButton mb:buttons)
					mb.Pressed(mx, my);
				premx=mx;
				premy=my;
				break;
			case MotionEvent.ACTION_UP:
				if(Math.abs(mx-premx)<hand_shake&&Math.abs(my-premy)<hand_shake){
					if(back.Release(mx, my))
						activity.handler.sendEmptyMessage(ViewId.MenuView);
					else if(soundS.Release(mx, my)){//声音设置
						currView=soundV;
					}else if(developerS.Release(mx, my)){//开发者设置
						currView=devV;
					}else if(aboutS.Release(mx, my)){
						currView=introV;
						introV.SetString(1);
					}else if(ruleS.Release(mx, my)){
						currView=introV;
						introV.SetString(0);
					}
					
				}
				break;
		}
		
		return true;
	}



	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(!isInit){
			back=new MyButton(0,0,200,160,R.drawable.ui_button_back);
			buttons.add(back);
			soundS=new MyButton(0,230,200,390, R.drawable.ui_setting_sound);
			buttons.add(soundS);
			developerS=new MyButton(0,460,200,620,R.drawable.ui_setting_developer);
			buttons.add(developerS);
			ruleS=new MyButton(0,690,200,850,R.drawable.ui_setting_rule);
			buttons.add(ruleS);
			aboutS=new MyButton(0,920,200,1080,R.drawable.ui_setting_about);
			buttons.add(aboutS);
			
			devV=new DeveloperView(context);
			soundV=new SoundView(context);
			introV=new IntroView(context);
			currView=soundV;
			
			isInit=true;
		}
		Draw(canvas);
		
		postInvalidate();
	}
	
	private void Draw(Canvas canvas){
	//	background.DrawSelf(canvas);
		canvas.drawBitmap(ui_background2, null, screenRect, null);
		for(MyButton mb:buttons) mb.DrawSelf(canvas);
		
		currView.Draw(canvas);
	}
	
}
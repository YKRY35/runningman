package com.Zhou.rm;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.RectF;
import android.opengl.GLUtils;
import static com.Zhou.rm.Constant.*;

public class DrawPerformance{
	
	private int texture;
	Bitmap bm;
	Canvas canvas;
	Paint textPaint;
//	float _x,_y,_ax,_ay;//逻辑坐标
	float _w,_h;
	float textHeight;
	GameLogic GmL;
	public DrawPerformance(){
		/*_w=SW/2;
		_h=SH/9;
		_x=mapL;
		_y=mapT-_h;
		_ax=mapL+_w;
		_ay=mapT;//适应手机*/
		_w=500;
		_h=200;
		textPaint=new Paint();
		textPaint.setColor(Color.CYAN);
		textPaint.setTextSize(30);
		textPaint.setStrokeWidth(20);
		FontMetrics fm=textPaint.getFontMetrics();
		textHeight=(float) Math.ceil(fm.descent-fm.top);
		
		GmL=GameLogic.GetInstance();
	//	bm=Bitmap.createBitmap((int)(_ax-_x), (int)(_ay-_y), Config.ARGB_4444);//应使用手机坐标！
	//	canvas=new Canvas(bm);
	//	canvas.clipRect(new RectF(_x,_ay,_ax,_y));
	}
	public void GenTexture(GL10 gl){
		int[] _textures=new int[1];
		gl.glGenTextures(1, _textures, 0);
		texture=_textures[0];
	}
	public void DrawSelf(GL10 gl){
		
		bm=Bitmap.createBitmap((int) _w, (int) textHeight+5, Config.ARGB_4444);//清空
		canvas=new Canvas(bm);
		textPaint.setTextAlign(Align.LEFT);
		String t1="距离"+IntToString((int)GmL.meters/6,9)+"米";
		canvas.drawText(t1,_w/8,textHeight,textPaint);

		textPaint.setTextAlign(Align.RIGHT);
		String t2="金币"+IntToString(GmL.man.moneyCnt,9)+"个";
		canvas.drawText(t2, _w, textHeight, textPaint);//android坐标
		
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, texture);
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bm, 0);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);										//游戏逻辑的坐标
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, GetVertexBuffer4(mapL,mapT-150,0,mapT-58,-1));//上边58
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, GetTexCoordBuffer());
		
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);//矩形
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
	private String IntToString(int num,int len){
		int numLen=0;
		String tmp="";
		int _num=num;
		while(num>0){
			num/=10;
			numLen++;
		}
		while(numLen<len){
			tmp+=" ";
			numLen++;
		}
		tmp+=String.valueOf(_num);
		return tmp;
	}
}
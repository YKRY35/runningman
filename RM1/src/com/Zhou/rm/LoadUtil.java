package com.Zhou.rm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import static com.Zhou.rm.Constant.*;

public class LoadUtil{
	public static void Load3DModelFromFile(Object3D object,SelectRoleView v){
		String objFilePath="model3D/"+object.dir+"/"+object.dir+".obj";
		String mtlFilePath="model3D/"+object.dir+"/"+object.dir+".mtl";
		
		HashMap<String,Integer> MTLToTId=new HashMap<String,Integer>();
		try{
			InputStream is=v.getResources().getAssets().open(mtlFilePath);
			InputStreamReader isr=new InputStreamReader(is);
			BufferedReader br=new BufferedReader(isr);
			String tmp=null;
			String currmtl=null;
			while((tmp=br.readLine())!=null){
				String[] tmpa=tmp.split("[ ]+");
				if(tmpa[0].trim().equals("newmtl")){
					currmtl=tmpa[1];
				}
				if(tmpa[0].trim().contains("map")){
					if(currmtl!=null){
						MTLToTId.put(currmtl, object.FNToTId.get(tmpa[1].trim()));
					}
				}
			}
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<Float> alv=new ArrayList<Float>();
		List<Float> alvResult=new ArrayList<Float>();
		List<Float> alt=new ArrayList<Float>();
		List<Float> altResult=new ArrayList<Float>();
		int lPoint=0,rPoint=0;
		int preTexId=-1;
		final float bigFloat=+10000000;
		final float smaFloat=-10000000;
		float maxX=smaFloat,minX=bigFloat;
		float maxY=smaFloat,minY=bigFloat;
		float maxZ=smaFloat,minZ=bigFloat;
		try {
			InputStream is=v.getResources().getAssets().open(objFilePath);
			InputStreamReader isr=new InputStreamReader(is);
			BufferedReader br=new BufferedReader(isr);
			String tmp=null;
			
			while((tmp=br.readLine())!=null){
				String[] tmpa=tmp.split("[ ]+");
				if(tmpa[0].trim().equals("v")){
					float x0=Float.parseFloat(tmpa[1]);
					float y0=Float.parseFloat(tmpa[2]);
					float z0=Float.parseFloat(tmpa[3]);
					if(x0>maxX) maxX=x0;
					if(x0<minX) minX=x0;
					if(y0>maxY) maxY=y0;
					if(y0<minY) minY=y0;
					if(z0>maxZ) maxZ=z0;
					if(z0<minZ) minZ=z0;
					
					alv.add(x0);
					alv.add(y0);
					alv.add(z0);
				}
				if(tmpa[0].trim().equals("vt")){
					alt.add(Float.parseFloat(tmpa[1]));
					alt.add(1-Float.parseFloat(tmpa[2]));
				}
				if(tmpa[0].trim().equals("usemtl")){//上一贴图的完结
					if(preTexId>=0){
						object.AddBlock(lPoint, rPoint, preTexId);
					}
					lPoint=rPoint;
					if( MTLToTId.containsKey(tmpa[1].trim()) ){
						preTexId=MTLToTId.get(tmpa[1].trim());
					}
				}
				if(tmpa[0].trim().equals("f")){
					for(int i=2;i+1<tmpa.length;i++){
						for(int j=1;j<=i+1;j++){
							String[] tmpaa=tmpa[j].split("/");
							if(j==1) j=i-1;
							
							int index=Integer.parseInt(tmpaa[0])-1;
							alvResult.add(alv.get(index*3));
							alvResult.add(alv.get(index*3+1));
							alvResult.add(alv.get(index*3+2));
							
							if(tmpaa[1].trim().equals("")){
								altResult.add(0.0f);
								altResult.add(0.0f);
							}else{
								index=Integer.parseInt(tmpaa[1])-1;
								altResult.add(alt.get(index*2));
								altResult.add(alt.get(index*2+1));
							}
							
							rPoint++;
						}
					}
					
				}
			}
			if(preTexId>=0){
				object.AddBlock(lPoint, rPoint, preTexId);
			}
			
			float[] vertex=new float[alvResult.size()];
			for(int i=0;i<alvResult.size();i++)
				vertex[i]=alvResult.get(i);
			ByteBuffer BB=ByteBuffer.allocateDirect(vertex.length*4);
			BB.order(ByteOrder.nativeOrder());
			object.vertexBuffer=BB.asFloatBuffer();
			object.vertexBuffer.put(vertex);
			object.vertexBuffer.position(0);
			object.vertexCount=vertex.length/3;
		//	Log.v("vertexCount",String.valueOf(object.vertexCount));
			
			float[] texCoor=new float[altResult.size()];
			for(int i=0;i<altResult.size();i++)
				texCoor[i]=altResult.get(i);
			BB=ByteBuffer.allocateDirect(texCoor.length*4);
			BB.order(ByteOrder.nativeOrder());
			object.texCoorBuffer=BB.asFloatBuffer();
			object.texCoorBuffer.put(texCoor);
			object.texCoorBuffer.position(0);
		//	Log.v("vertexCount",String.valueOf(altResult.size()/2));
			
			object.maxX=maxX;
			object.minX=minX;
			object.midX=(maxX+minX)/2;
			object.maxY=maxY;
			object.minY=minY;
			object.midY=(maxY+minY)/2;
			object.maxZ=maxZ;
			object.minZ=minZ;
			object.midZ=(maxZ+minZ)/2;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static Bitmap LoadBitmapFromAsset(String filePath,SelectRoleView v){
		Bitmap bitmap = null;
		try {
			InputStream is=v.getResources().getAssets().open(filePath);
			bitmap=BitmapFactory.decodeStream(is);
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bitmap;
	}
	
	public static String[] GetAllFilesFromAsset(String filePath,SelectRoleView v){
		String[] files=null;
		try {
			files=v.getResources().getAssets().list(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return files;
	}
	
	public static int getBitmapSize(Bitmap bitmap){
		 /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){     //API 19
		        return bitmap.getAllocationByteCount();
		    }*/
		    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1){//API 12
		        return bitmap.getByteCount();
		    }
		    return bitmap.getRowBytes() * bitmap.getHeight();                //earlier version
		}//此函数来自网上
		
		public static Bitmap ScalingBitmap(Bitmap bitmap,float dst_width,float dst_height){
			int width=bitmap.getWidth();
			int height=bitmap.getHeight();
			Matrix m=new Matrix();
			float ratioW=dst_width/(float)width;
			float ratioH=dst_height/(float)height;
			if(ratioW>1) ratioW=1;
			if(ratioH>1) ratioH=1;
			m.postScale(ratioW, ratioH);
			Bitmap rlt_bitmap=Bitmap.createBitmap(bitmap, 0, 0, width, height, m, true);
			return rlt_bitmap;
		}
		
	/*	public static Bitmap MyDecodeResource(int resId, float dst_width, float dst_height){
			BitmapFactory.Options option=new BitmapFactory.Options();
			option.inJustDecodeBounds=true;
			BitmapFactory.decodeResource(resources, resId, option);
			int img_width=option.outWidth;
			int img_height=option.outHeight;
			int inSampleSize=1;
			if(img_width>dst_width||img_height>dst_height){
				int ratio_width=Math.round( (float)img_width/(float)dst_width );
				int ratio_height=Math.round( (float)img_height/(float)dst_height );
				inSampleSize=Math.min(ratio_width, ratio_height);
			}
			option.inSampleSize=inSampleSize;
			option.inJustDecodeBounds=false;
			return BitmapFactory.decodeResource(resources, resId, option);
		}//部分设备可能一加载没来得及压缩就OOM，所以用此方法在加载时就缩放
		*/
		public static Bitmap MyDecodeStream(int resId, float dst_width, float dst_height){
			InputStream is=resources.openRawResource(resId);
		//	return BitmapFactory.decodeStream(is);
			return ScalingBitmap(BitmapFactory.decodeStream(is), dst_width ,dst_height);
		}
		public static Bitmap MyDecodeStream(int resId){
			InputStream is=resources.openRawResource(resId);
			return BitmapFactory.decodeStream(is);
		}
}
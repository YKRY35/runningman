precision mediump float;

uniform sampler2D vTexture;
varying vec2 aCoordinate;
//varying vec4 aaColor; //接收从顶点着色器过来的参数

void main(){
    gl_FragColor=texture2D(vTexture,aCoordinate);
//	gl_FragColor=aaColor;
}

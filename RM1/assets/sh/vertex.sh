attribute vec3 aPosition;
attribute vec2 aTexture;
//attribute vec4 aColor;
uniform mat4 uMVPMatrix;

varying vec2 aCoordinate;
//varying vec4 aaColor;
void main(){
	gl_Position=uMVPMatrix*vec4(aPosition,1);
	aCoordinate=aTexture;
//	aaColor=aColor;
}